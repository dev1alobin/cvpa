﻿using Cvpa.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cvpa
{
    public class FilterConfig
    {
        public static void RegisterGlobalFitlers(GlobalFilterCollection filters)
        {
            //filters.Add(new AloErrorHandleAttribute());
            filters.Add(new AssociationAuthorizeAttribute());
        }
    }
}