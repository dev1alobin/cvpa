﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cvpa.Models;
using Cvpa.DALs;
using PetaPoco;
using Cvpa.Utils;
using System.IO;

namespace Cvpa.BALs
{
    public class MemberBAL
    {
        private MemberDAL memberDAL;
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(MemberBAL));
      //  private static string Member_Image_Path = ControllerUtils.GetScreenData("Member", "Member_Image_Upload");

        public MemberBAL()
        {
            try
            {
                this.memberDAL = new MemberDAL();
            }
            catch (Exception e)
            {
            }
        }

        public IEnumerable<Member> All()
        {
            return this.memberDAL.All();
        }

        public Sql GenerateQuery(MemberListViewModel memberListViewModel)
        {
            var query = new Sql("Select * from Members");
            var memberFilter = memberListViewModel.MemberFilter;
            int value;
            if ((int.TryParse(memberFilter.Query, out value)) && ((memberFilter.Query).Length <= 6))
            {
                query.Where("MemberNo = @0 or StorePincode = @0", memberFilter.Query);
            }
            if (!string.IsNullOrEmpty(memberFilter.Query))
            {
                query.Where("((MemberName like @0) OR (MemberNo like @0) or (SpecialistIn like @0) or (MobileNo like @0) or (StoreArea like @0) or (StorePincode like @0) or (Community like @0) or (MemberPost like @0) or (MembershipPaid like @1) OR (Zone like @0))"
                    , "%" + memberFilter.Query + "%"
                    , memberFilter.Query);
            }
            if (memberFilter != null && !String.IsNullOrEmpty(memberFilter.MemberType) && memberFilter.MemberType.Equals("LifeMembers"))
            {
                query.Where("MemberType = @0", "LIFE MEMBER");
            }
            if (memberFilter != null && !String.IsNullOrEmpty(memberFilter.MemberType) && memberFilter.MemberType.Equals("ActiveMembers"))
            {
                query.Where("@0 BETWEEN MembershipStartDate AND MembershipExpiryDate", DateTime.Now.Date);
            }
            if (memberFilter.Zone != null) {
                query.Where(@"Zone = @0", memberFilter.Zone);
            }
            if (memberFilter.StoreArea != null)
            {
                query.Where(@"StoreArea = @0", memberFilter.StoreArea);
            }
            if (memberFilter != null && !String.IsNullOrEmpty(memberFilter.MemberType) && memberFilter.MemberType.Equals("ExpiredMembers"))
            {
                query.Where("MembershipExpiryDate < @0 OR MembershipExpiryDate is null", DateTime.Now.Date);
            }
            if (memberFilter.FromDateTime != null && memberFilter.ToDateTime != null)
            {
                query.Where(@"MembershipExpiryDate >= @0 and MembershipExpiryDate <= @1"
                                , memberFilter.FromDateTime, memberFilter.ToDateTime);
            }
            else if (memberFilter.FromDateTime != null)
            {
                query.Where(@"MembershipExpiryDate >= @0", memberFilter.FromDateTime);
            }
            else if (memberFilter.ToDateTime != null)
            {
                query.Where(@"MembershipExpiryDate <= @0", memberFilter.ToDateTime);
            }
            query.Where("Status = @0", "1");
            query.OrderBy("MemberNo DESC");
            return query;
        }

        public Page<Member> PagedQuery(MemberListViewModel memberListViewModel)
        {
            try
            {
                var query = this.GenerateQuery(memberListViewModel);
                return this.memberDAL.PagedQuery<Member>(memberListViewModel.PageNumber, memberListViewModel.ItemsPerPage, query);
            }
            catch (Exception e) {
                logger.Info("Error in Paged Query " + e.Message);
            }
            return null;
        }


         public int GetLastMemberID() {
            int lastmemberid = 0;
            try
            {
                lastmemberid =  this.memberDAL.GetLastMemberID();
            }
            catch (Exception e) {
                logger.Info("Error in GetLastMemberID" + e.Message);
            }
            return lastmemberid;
        }
        public void Save(Member member)
        {
            try
            {
                var Member_Image_Path = ControllerUtils.GetScreenData("Member", "Member_Image_Upload");
                long LastMemberID ;
                LastMemberID = this.memberDAL.GetLastMemberID();
                logger.Info("LastMemberID =" +LastMemberID);
                member.MemberNo = LastMemberID + 1;
                //member.MembershipPaid = "Paid";
                member.Status = 1;
                var ScanFileName = "";
                var ScannedFileDir = Member_Image_Path;
                ScanFileName = member.MemberNo + " P.jpg";

              //  ScanFileName = member.MemberNo + " P";
                    //this.UploadImage(member.FileData, ScanFileName);
                    if (!Directory.Exists(ScannedFileDir))
                    {
                        Directory.CreateDirectory(ScannedFileDir);
                    }
                    if (member.FileData != null) {
                        var ScannedFilePath = ScannedFileDir + ScanFileName;
                        member.FileData.SaveAs(ScannedFilePath);
                        member.PhotoName = ScanFileName;
                    }
                
                this.memberDAL.Save(member);
            }
            catch (Exception e)
            {
                logger.Info("Error In Save Member" + e.Message);
            }
        }
        public void Update(Member member)
        {
            try
            {
                var Member_Image_Path = ControllerUtils.GetScreenData("Member", "Member_Image_Upload");
                logger.Info("Inside Update Member Bal MemberID = " + member.MemberID);
                var ScanFileName = "";
                var ScannedFileDir = Member_Image_Path;
                ScanFileName = member.MemberNo + " P.jpg";

              //  ScanFileName = member.MemberNo + " P";
                //this.UploadImage(member.FileData, ScanFileName);
                if (!Directory.Exists(ScannedFileDir))
                {
                    Directory.CreateDirectory(ScannedFileDir);
                }
                if (member.FileData != null)
                {
                    var ScannedFilePath = ScannedFileDir + ScanFileName;
                    member.FileData.SaveAs(ScannedFilePath);
                    member.PhotoName = ScanFileName;
                }
                member.Status = 1;
                this.memberDAL.Update(member);
            }
            catch (Exception e) 
            {
                logger.Info("Error in Update Member" + e.Message);
            }
        }
        public int Delete(long ID)
        {
            int deletemember= 0;
            try
            {
               
               deletemember = this.memberDAL.Delete(ID);
            }
            catch (Exception e)
            {
                logger.Info("Error in Member Delete" + e.Message);
            }
            return deletemember;
        }
        public int Dismiss(long ID, string Reason)
        {
            int dismissmember = 0;
            try
            {

                dismissmember = this.memberDAL.Dismiss(ID,Reason);
            }
            catch (Exception e)
            {
                logger.Info("Error in Member Dismiss" + e.Message);
            }
            return dismissmember;
        }

        public Member FindByID(long MemberID)
        {
            Member memberdetails = new Member();
            try
            {
                logger.Info("Inside FindByID MemberID = "+MemberID);
                memberdetails = this.memberDAL.FindByID(MemberID);
            }
            catch (Exception e)
            {
                logger.Info("Error in FindByID " + e.Message);
            }
            return memberdetails;
        }

        public Member FindByMemberNo(long MemberNo)
        {
            Member memberdetails = new Member();
            try
            {
                logger.Info("Inside FindBy MemberNo = " + MemberNo);
                memberdetails = this.memberDAL.FindByMemberNo(MemberNo);
            }
            catch (Exception e)
            {
                logger.Info("Error in FindBy MemberNo " + e.Message);
            }
            return memberdetails;
        }


        public List<Member> GetMembershipNo(long MembershipNo)
        {
            List<Member> memberdetails = new List<Member>();
            logger.Info("Inside Get MembershipNo" + MembershipNo);
            try
            {
                
                memberdetails = this.memberDAL.GetMembershipNo(MembershipNo);
                logger.Info("GetMemberShipNo " + memberdetails.ToString());
            }
            catch (Exception e)
            {
                logger.Info("Error in GetMemberShipNo" + e.Message);
            }
            return memberdetails;
        }

        public List<Member> GetMemberNoForMeeting(string ColumnName, long term)
        {            
            List<Member> meetingautocomplete = new List<Member>();
            logger.Info("Inside Get GetMemberAutoComplete" + ColumnName);
            try
            {

                meetingautocomplete = this.memberDAL.GetMemberNoForMeeting(ColumnName, term);
                logger.Info("GetMembershipNoMeeting " + meetingautocomplete.Count);
            }
            catch (Exception e)
            {
                logger.Info("Error in GetMemberAutoComplete" + e.Message);
            }
            return meetingautocomplete;
        }

        public List<Member> MembersPrint(MemberListViewModel memberListViewModel)
        {
            List<Member> memberdetails = new List<Member>();
            try
            {
                var query = this.GenerateQuery(memberListViewModel);
                memberdetails = this.memberDAL.MembersPrint(query);
            }
            catch (Exception e)
            {
                logger.Info("Error in MembersPrint" + e.Message);
            }
            return (memberdetails);
        }



        public int GetExpiredMembersCount()
        {
            int membercount = 0;
            try
            {
                logger.Info("Inside GetExpiredMemberCount");
                membercount = this.memberDAL.GetExpiredMembersCount();
            }
            catch (Exception e)
            {
                logger.Info("Error in GetMemberCount = " + e.Message);
            }
            return membercount;
        }

        public int GetMembersCount()
        {
            int membercount = 0;
            try
            {
                logger.Info("Inside GetMemberCount");
                membercount = this.memberDAL.GetMembersCount();
            }
            catch (Exception e)
            {
                logger.Info("Error in GetMemberCount = " + e.Message);
            }

            return membercount;
        }

        public int GetLifeMemberCount()
        {
            int membercount = 0;
            try
            {
                logger.Info("Inside GetLifeMemberCount");
                membercount = this.memberDAL.GetLifeMemberCount();
            }
            catch (Exception e)
            {
                logger.Info("Error in GetLifeMemberCount = " + e.Message);
            }

            return membercount;
        }

        public int GetActiveMembersCount()
        {
            return this.memberDAL.GetActiveMembersCount();
        }

        public List<Member> GetMembersExpiry()
        {
            List<Member> expirycount = new List<Member>();
            try
            {
                logger.Info("Inside GetExpiryCount");
                expirycount = this.memberDAL.GetMembersExpiry();
            }
            catch (Exception e)
            {
                logger.Info("Error in GetExpiryCount = " + e.Message);
            }

            return expirycount;
        }

        public List<string> GetMemberAutoComplete(string ColumnName , string term)
        {
            List<string> memberautocomplete = new List<string>();
            logger.Info("Inside Get GetMemberAutoComplete" + ColumnName);
            try
            {

                memberautocomplete = this.memberDAL.GetMemberAutoComplete(ColumnName, term);
                logger.Info("GetMemberAutoComplete " + memberautocomplete.ToString());
            }
            catch (Exception e)
            {
                logger.Info("Error in GetMemberAutoComplete" + e.Message);
            }
            return memberautocomplete;
        }

        public Member GetMemberForReceiptInsert(long memberid) {
            Member Memberdetails = new Member();
            try
            {
                logger.Info("Inside GetMemberForReceiptInsert where MembershipNo = " + memberid);
                Memberdetails = this.memberDAL.GetMemberForReceiptInsert(memberid);
            }
            catch (Exception e) {
                logger.Info("Error in GetMemberForReceiptInsert" + e.Message);
            }
            
            return Memberdetails;
        }

        public Member GetMembersExpiryDate(long memberID)
        {
            Member Memberdetails = new Member();
            try
            {
                logger.Info("Inside GetMemberForReceiptInsert where MembershipNo = " + memberID);
                Memberdetails = this.memberDAL.GetMemberForReceiptInsert(memberID);
            }
            catch (Exception e)
            {
                logger.Info("Error in GetMemberForReceiptInsert" + e.Message);
            }

            return Memberdetails;
        }
        public List<Member> SendSms(string SmsTo, string Area)
        {
            List<Member> Memberdetails = new List<Member>();
            try
            {
                logger.Info("Inside SendSMS BAL where SMSTO = " + SmsTo + " & Area = "+ Area);
                Memberdetails = this.memberDAL.SendSms(SmsTo, Area);
            }
            catch (Exception e)
            {
                logger.Info("Error in SendSMS" + e.Message);
            }

            return Memberdetails;
        }

        public List<string> MembersAreaList(string ColumnName)
        {
            List<string> membersarealist = new List<string>();
            logger.Info("Inside Get MembersAreaList" + ColumnName);
            try
            {

                membersarealist = this.memberDAL.MembersAreaList(ColumnName);
                logger.Info("MembersAreaList " + membersarealist.ToString());
            }
            catch (Exception e)
            {
                logger.Info("Error in MembersAreaList" + e.Message);
            }
            return membersarealist;
        }

        public List<string> GetZoneList() {
            List<string> ZoneList = new List<string>();
            try {
                ZoneList = this.memberDAL.GetZoneList();
            }
            catch (Exception e) {
                logger.Info("Error in GetZoneList BAL " + e.Message);
            }
            return ZoneList;
        }

        public List<string> GetAreaList(string ZoneName)
        {
            List<string> AreaList = new List<string>();
            try
            {
                AreaList = this.memberDAL.GetAreaList(ZoneName);
            }
            catch (Exception e)
            {
                logger.Info("Error in GetAreaList BAL " + e.Message);
            }
            return AreaList;
        }

        public List<string> MembersAreaListByArea(string term, string areaType)
        {
            List<string> membersarealist = new List<string>();
            logger.Info("Inside Get MembersAreaList" + term + ", Area Type " + areaType);
            try
            {

                membersarealist = this.memberDAL.MembersAreaListByArea(term, areaType);
                logger.Info("MembersAreaList " + membersarealist.ToString());
            }
            catch (Exception e)
            {
                logger.Info("Error in MembersAreaList" + e.Message);
            }
            return membersarealist;
        }

        public List<Member> GetMembersForInsurence()
        {
            return this.memberDAL.GetMembersForInsurence();
        }

    }

}