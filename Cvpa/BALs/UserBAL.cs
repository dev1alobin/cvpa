﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cvpa.Models;
using Cvpa.DALs;

namespace Cvpa.BALs
{
    public class UserBAL
    {
        private UserDAL userDAL;
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(UserBAL));
      
        public UserBAL() {

            try
            {
                this.userDAL = new UserDAL();
            }
            catch(Exception e) { 
                
            }
        }

        public User AuthenticateUser(User user)
        {
            User LoggedUser = null;
            try
            {
                LoggedUser = this.userDAL.AuthenticateUser(user);
            }
            catch (Exception e)
            {
                logger.Error("Error Occurred in AuthenticateUser " + e.Message);
                logger.Error("Error Object for AuthenticateUser " + e);
            }
            return LoggedUser;
        }

       

    }
}