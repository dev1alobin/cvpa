﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cvpa.Models;
using Cvpa.DALs;
using PetaPoco;
using log4net;
using Cvpa.Utils;
namespace Cvpa.BALs
{
    public class ReceiptBAL
    {
        private ReceiptDAL receiptDAL;
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(ReceiptBAL));
        private ScreencustomizationBAL screencustomizationBAL = new ScreencustomizationBAL();
        public ReceiptBAL()
        {
            try
            {
                this.receiptDAL = new ReceiptDAL();
            }
            catch (Exception e)
            {
            }
        }

        public IEnumerable<Receipt> All()
        {
            return new List<Receipt>()
            {
                new Receipt() {
                }
            };
        }

        public Sql GenerateQuery(ReceiptListViewModel receiptListViewModel)
        {
            var receiptFilter = receiptListViewModel.ReceiptFilter;
            var query = new Sql("Select Receipts.*,Members.* from receipts");
            query.LeftJoin("Members").On("Members.MemberID = Receipts.MemberID");

            int value;
            if (int.TryParse(receiptListViewModel.Query, out value))
            {
                query.Where("Members.MemberNo = @0", receiptListViewModel.Query);
            }
            if ((!string.IsNullOrEmpty(receiptListViewModel.Query)))
            {
                query.Where("Members.MemberNo like @0 or Receipts.ReceiptNo like @0 or Members.MemberName like @0"
                            , "%" + receiptListViewModel.Query + "%"
                            , 1);
            }
            if (receiptFilter != null && receiptFilter.FromDateTime != null && receiptFilter.ToDateTime != null)
            {
                query.Where("ReceiptDate between @0 and @1"
                    , receiptFilter.FromDateTime
                    , receiptFilter.ToDateTime);
            }
            else if (receiptFilter != null && receiptFilter.FromDateTime != null)
            {
                query.Where("ReceiptDate >= @0", receiptFilter.FromDateTime);
            }
            else if (receiptFilter != null && receiptFilter.ToDateTime != null)
            {
                query.Where("ReceiptDate <= @0", receiptFilter.ToDateTime);
            }
            query.Where("Receipts.Status = @0", 1);
            query.OrderBy("ReceiptDate Desc");
            return query;
        }

        public Page<Receipt> PagedQuery(ReceiptListViewModel receiptListViewModel)
        {
            logger.Info("Receipt Page Query");
            try
            {
                var query = this.GenerateQuery(receiptListViewModel);
                return this.receiptDAL.PagedQuery<Receipt>(receiptListViewModel.PageNumber, receiptListViewModel.ItemsPerPage, query);
            }

            catch (Exception e)
            {
                logger.Info("Error in Receipt Search" + e.Message);
            }
            return null;
        }

        public void Save(Receipt receipt)
        {
            try
            {
                int getlastreceiptno = 0;
                try
                {
                    getlastreceiptno = this.receiptDAL.GetLastReceiptNo();
                }
                catch (Exception e)
                {

                    logger.Info("No Records Found" + e.Message);
                }
                logger.Info("Inside SaveReceipt BAL " + receipt.ToString());
                logger.Info("Inside GetLastReceiptNo BAL " + getlastreceiptno);
                receipt.ReceiptNo = getlastreceiptno + 1;
                this.receiptDAL.Save(receipt);
            }
            catch (Exception e)
            {
                logger.Info("Error in SaveReceipt BAL" + e.Message);
            }
        }

        public void Update(Receipt receipt)
        {
            try
            {
                logger.Info("Inside Update Receipt BAL " + receipt.ToString());
                this.receiptDAL.Update(receipt);
            }
            catch (Exception e)
            {
                logger.Info("Error in Update Receipt BAL " + e.Message);
            }
        }

        public void Delete(Receipt receipt)
        {
            try
            {
                this.receiptDAL.Delete(receipt);
            }
            catch (Exception e)
            {
            }
        }

        public Receipt FindByID(long receiptID)
        {
            try
            {
                logger.Info("Inside Receipt FindByID = " + receiptID);
                return this.receiptDAL.FindByID(receiptID);
            }
            catch (Exception e)
            {
                logger.Info("Error in Receipt FindByID =" + e.Message);
            }
            return null;
        }

        public List<Receipt> ReceiptPrint(ReceiptListViewModel receiptListViewModel)
        {
            List<Receipt> receiptdetails = new List<Receipt>();
            try
            {
                var query = this.GenerateQuery(receiptListViewModel);
                receiptdetails = this.receiptDAL.ReceiptPrint(query);
            }
            catch (Exception e)
            {
                logger.Info("Error in MembersPrint" + e.Message);
            }
            return (receiptdetails);
        }

        public List<Receipt> GetReceiptsCount()
        {
            List<Receipt> receiptcount = new List<Receipt>();
            try
            {
                logger.Info("Inside GetReceiptCount ");
                receiptcount = this.receiptDAL.GetReceiptsCount();
            }
            catch (Exception e)
            {
                logger.Info("Error in GetReceiptCount" + e.Message);
            }
            return (receiptcount);
        }
        public Receipt ReceiptSinglePrint(long ReceiptID, long MemberID)
        {
            try
            {
                logger.Info("Inside Receipt ReceiptSinglePrint = " + ReceiptID + "& MemberID = " + MemberID);
                return this.receiptDAL.ReceiptSinglePrint(ReceiptID, MemberID);
            }
            catch (Exception e)
            {
                logger.Info("Error in Receipt ReceiptSinglePrint =" + e.Message);
            }
            return null;
        }

        public void SaveMember(Receipt receipt)
        {
            try
            {
                logger.Info("Inside SaveMember");
                //  this.receiptDAL.SaveMember(receipt);
            }
            catch (Exception e)
            {
                logger.Info("Error in SaveMember " + e.Message);
            }
        }
    }
}