﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cvpa.Models;
using Cvpa.DALs;
using PetaPoco;
using log4net;
using Cvpa.Utils;
namespace Cvpa.BALs
{
    public class MagazineBAL
    {
        private MagazineDAL magazineDAL;
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(InsuranceBAL));
        private ScreencustomizationBAL screencustomizationBAL = new ScreencustomizationBAL();
        public MagazineBAL()
        {
            try
            {
                this.magazineDAL = new MagazineDAL();
            }
            catch (Exception e)
            {
            }
        }

        public IEnumerable<Magazine> All()
        {
            return new List<Magazine>()
            {
                new Magazine() {
                }
            };
        }

        public Sql GenerateQuery(MagazineListViewModel magazineListViewModel)
        {
            var query = new Sql("Select * from Magazines");
            int value;
            if ((int.TryParse(magazineListViewModel.Query, out value)) && ((magazineListViewModel.Query).Length <= 6))
            {
                query.Where("MemberNo = @0 or Pincode = @0", magazineListViewModel.Query);
            }
            else if ((!string.IsNullOrEmpty(magazineListViewModel.Query)))
            {
                query.Where("Name like @0 or MobileNo like @0 or TelephoneNo like @0 or Area like @0 or City like @0", "%" + magazineListViewModel.Query + "%");
            }
            query.Where("Status = @0", 1);
            return query;
        }

        public Page<Magazine> PagedQuery(MagazineListViewModel magazineListViewModel)
        {
            logger.Info("Magazine Page Query");
           
            try {
                var query = this.GenerateQuery(magazineListViewModel);
                return this.magazineDAL.PagedQuery<Magazine>(magazineListViewModel.PageNumber, magazineListViewModel.ItemsPerPage, query);
            }
            
            catch (Exception e)
            {
                logger.Info("Error in Insurance Search" + e.Message);
            }
            return null;
        }

        public void Save(Magazine magazine)
        {
            try
            {
                this.magazineDAL.Save(magazine);
            }
            catch (Exception e)
            {
                logger.Info("Error in Save Magazine BAL" + e.Message);
            }
        }

        public void Update(Magazine magazine)
        {
            try
            {
                logger.Info("Inside Update magazine BAL " + magazine.ToString());
                this.magazineDAL.Update(magazine);
            }
            catch (Exception e)
            {
                logger.Info("Error in Update magazine BAL " + e.Message);
            }
        }

        public int Delete(long ID)
        {
            try
            {
              return  this.magazineDAL.Delete(ID);
            }
            catch (Exception e)
            {
                logger.Info("Error in Delete Magazine " +e.Message);
            }
            return 0;
        }

        public Magazine FindByID(long magazineID)
        {
            try
            {
                logger.Info("Inside Insurance FindByID = " + magazineID);
                return this.magazineDAL.FindByID(magazineID);
            }
            catch (Exception e)
            {
                logger.Info("Error in Insurance FindByID =" + e.Message);
            }
            return null;
        }

        public List<Magazine> AddressPrint(string Query, string PrintOption)
        {
            List<Magazine> memberdetails = new List<Magazine>();
            try
            {
                logger.Info("Inside MagazineAddressPrint =" + Query + " PrintOption = " + PrintOption);
                memberdetails = this.magazineDAL.AddressPrint(Query, PrintOption);
            }
            catch (Exception e)
            {
                logger.Info("Error in MagazineAddressPrint" + e.Message);
            }
            return (memberdetails);
        }
    }
}