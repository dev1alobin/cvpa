﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cvpa.Models;
using Cvpa.DALs;
using PetaPoco;
using log4net;
using Cvpa.Utils;
namespace Cvpa.BALs
{
    public class InsuranceBAL
    {
        private InsuranceDAL insuranceDAL;
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(InsuranceBAL));
        private ScreencustomizationBAL screencustomizationBAL = new ScreencustomizationBAL();
        public InsuranceBAL()
        {
            try
            {
                this.insuranceDAL = new InsuranceDAL();
            }
            catch (Exception e)
            {
            }
        }

        public IEnumerable<Insurance> All()
        {
            return new List<Insurance>()
            {
                new Insurance() {
                }
            };
        }

        public Sql GenerateQuery(InsuranceListViewModel receiptListViewModel)
        {
            var insuranceFilter = receiptListViewModel.InsuranceFilter;
            var query = new Sql("Select Insurances.*, Members.MemberNo, Members.MemberName, Members.MobileNo, Members.NomineeName, Members.Relationship from Insurances");
            query.LeftJoin("Members").On("Members.MemberID = Insurances.MemberID");

            int value;
            if ((int.TryParse(receiptListViewModel.Query, out value)) && ((receiptListViewModel.Query).Length <= 6))
            {
                query.Where("Members.MemberNo = @0 or Members.StorePincode = @0 or PolicyNo = @0", receiptListViewModel.Query);
            }
            if ((!string.IsNullOrEmpty(receiptListViewModel.Query)))
            {
                query.Where("PolicyNo like @0 or Members.MemberNo like @0 or Members.MemberName like @0"
                    , "%" + receiptListViewModel.Query + "%");
            }
            if (insuranceFilter != null && insuranceFilter.FromDateTime != null && insuranceFilter.ToDateTime != null)
            {
                query.Where("ReceiptToYear between @0 and @1", insuranceFilter.FromDateTime, insuranceFilter.ToDateTime);
            }
            else if (insuranceFilter != null && insuranceFilter.FromDateTime != null)
            {
                query.Where("ReceiptToYear >= @0", insuranceFilter.FromDateTime);
            }
            else if (insuranceFilter != null && insuranceFilter.ToDateTime != null)
            {
                query.Where("ReceiptToYear <= @0", insuranceFilter.ToDateTime);
            }
            query.Where("Insurances.Status = @0", "1");
            query.OrderBy("Members.MemberNo asc");
            return query;
        }

        public Page<Insurance> PagedQuery(InsuranceListViewModel receiptListViewModel)
        {
            logger.Info("Insurance Page Query");
           
            try {
                var query = this.GenerateQuery(receiptListViewModel);
                return this.insuranceDAL.PagedQuery<Insurance>(receiptListViewModel.PageNumber, receiptListViewModel.ItemsPerPage, query);                
            }
            
            catch (Exception e)
            {
                logger.Info("Error in Insurance Search" + e.Message);
            }
            return null;
        }

        public void Save(Insurance insurance)
        {
            try
            {
                int getlastreceiptno = 0;
                try
                {
                 //   getlastreceiptno = this.insuranceDAL.GetLastInsuranceNo();
                }
                catch (Exception e){
                    logger.Info("No Records Found"+e.Message);
                }
                logger.Info("Inside SaveInsurance BAL " + insurance.ToString());
                logger.Info("Inside GetLastInsuranceNo BAL " + getlastreceiptno);
               // insurance.InsuranceID = getlastreceiptno + 1;
                this.insuranceDAL.Save(insurance);
            }
            catch (Exception e)
            {
                logger.Info("Error in SaveInsurance BAL" + e.Message);
            }
        }

        public void Update(Insurance insurance) {
            try
            {
                logger.Info("Inside Update Insurance BAL " + insurance.ToString());
                this.insuranceDAL.Update(insurance);
            }
            catch (Exception e){
                logger.Info("Error in Update Insurance BAL " + e.Message);
            }
        }

        public void Delete(Insurance insurance)
        {
            try
            {
                this.insuranceDAL.Delete(insurance);
            }
            catch (Exception e)
            {
            }
        }

        public Insurance FindByID(long receiptID)
        {
            try
            {
                logger.Info("Inside Insurance FindByID = " + receiptID);
                  return this.insuranceDAL.FindByID(receiptID);
            }
            catch (Exception e)
            {
                logger.Info("Error in Insurance FindByID =" + e.Message);
            }
            return null;
        }

        public List<Insurance> InsuranceReceiptListPrint(InsuranceListViewModel receiptListViewModel)
        {
            List<Insurance> insurancedetails = new List<Insurance>();
            try
            {
                var query = this.GenerateQuery(receiptListViewModel);
                insurancedetails = this.insuranceDAL.InsuranceReceiptListPrint(query);
            }
            catch (Exception e)
            {
                logger.Info("Error in InsurancePrint" + e.Message);
            }
            return (insurancedetails);
        }

        public List<Insurance> GetInsurancesCount()
        {
            List<Insurance> insurancecount = new List<Insurance>();
            try
            {
                logger.Info("Inside GetInsuranceCount " );
                insurancecount = this.insuranceDAL.GetInsurancesCount();
            }
            catch (Exception e)
            {
                logger.Info("Error in GetInsuranceCount" + e.Message);
            }
            return (insurancecount);
        }
        public Insurance InsuranceReceiptSinglePrint(long InsuranceID, long MemberID)
        {
            try
            {
                logger.Info("Inside Insurance InsuranceReceiptSinglePrint = " + InsuranceID + "& MemberID = " + MemberID);
                return this.insuranceDAL.InsuranceReceiptSinglePrint(InsuranceID, MemberID);
            }
            catch (Exception e)
            {
                logger.Info("Error in Insurance InsuranceSinglePrint =" + e.Message);
            }
            return null;
        }

        public void SaveMember(Insurance receipt) 
        {
            try {
                logger.Info("Inside SaveMember");
              //  this.receiptDAL.SaveMember(receipt);
            }
            catch (Exception e) {
                logger.Info("Error in SaveMember " + e.Message);
            }
        }

        public void InsertInsurance(List<Member> members)
        {
            this.insuranceDAL.InsertInsurance(members);
        }
    }
}