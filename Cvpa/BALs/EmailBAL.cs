﻿using AloFramework.Core.Models;
using Cvpa.DALs;
using Cvpa.Models;
using Cvpa.Utils;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cvpa.BALs
{
    public class EmailBAL
    {
        private EmailDAL _emailDAL = new EmailDAL();

        public Page<Email> PagedQuery(EmailListViewModel emailListViewModel)
        {
            return _emailDAL.PagedQuery(emailListViewModel);
        }

        public Email Save(Email email)
        {
            return _emailDAL.Save(email);
        }
        
        public Email GetEmail(long? ID)
        {
            return _emailDAL.GetEmail((long)ID);
        }

        public void Delete(long? ID)
        {
             this._emailDAL.Delete(ID);
        }

        public IEnumerable<Email> GetEmails(string filter)
        {
            return _emailDAL.GetEmails(filter);
        }
    }
}