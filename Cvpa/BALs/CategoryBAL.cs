﻿using Cvpa.Controllers;
using Cvpa.DALs;
using Cvpa.Models;
using PetaPoco;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cvpa.BALs
{
    public class CategoryBAL
    {
        private CategoryDAL categoryDAL = new CategoryDAL();
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(CategoryController));

        public List<Category> GetCategoriesByType(string categorytype)
        {
            var category = this.categoryDAL.GetCategoriesByType(categorytype);
            return category;
        }

        public void Save(Category category)
        {
            try
            {
                this.categoryDAL.Save(category);
            }
            catch (Exception e)
            {
                logger.Info("Error Message in Category BAL" + e.Message);
            }
        }
        public Page<Category> PagedQuery(CategoryListViewModel categoryListViewModel)
        {
            try
            {
                var MeetingList = this.categoryDAL.PagedQuery<Category>(categoryListViewModel.PageNumber, categoryListViewModel.ItemsPerPage, "Select * from categorys where status = @0 and CategoryType != @1", 1, "Receipt");
                if (!String.IsNullOrEmpty(categoryListViewModel.Query))
                {
                    MeetingList = this.categoryDAL.PagedQuery<Category>(categoryListViewModel.PageNumber, categoryListViewModel.ItemsPerPage, "Select * from categorys where (status = @0 and CategoryType != @1) and CategoryName like @2 ", 1, "Receipt", "%" + categoryListViewModel.Query + "%");
                }
                return MeetingList;
            }
            catch (Exception e)
            {
                logger.Info("Error message in catgegorylist BAL" + e.Message);
            }
            return null;
        }
        
        public void Update(Category category)
        {
            try
            {
                this.categoryDAL.Update(category);
            }
            catch (Exception e)
            {
                logger.Info("Error Message in Category BAL" + e);
            }
        }
        

        public Category FindByID(long CategoryID)
        {
            Category meetingdetails = new Category();
            try
            {
                logger.Info("Inside FindByID MeetingID = " + CategoryID);
                meetingdetails = this.categoryDAL.FindByID(CategoryID);
            }
            catch (Exception e)
            {
                logger.Info("Error in FindByID Category BAL " + e.Message);
            }
            return meetingdetails;
        }

        public int Delete(long ID)
        {
            int deletecategory = 0;
            try
            {
                deletecategory = this.categoryDAL.Delete(ID);
            }
            catch (Exception e)
            {
                logger.Info("Error in Category Delete" + e.Message);
            }
            return deletecategory;
        }
    }
}