﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cvpa.Models;
using Cvpa.DALs;

namespace Cvpa.BALs
{
    public class ScreencustomizationBAL
    {
        private ScreencustomizationDAL screencustomizationDAL;
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(MemberBAL));

        public ScreencustomizationBAL()
        {
            try
            {
                this.screencustomizationDAL = new ScreencustomizationDAL();
            }
            catch (Exception e)
            {
            }
        }
        public Screencustomization GetScreenData(string ScreenName, string Segment)
        {
            logger.Info("inside GetScreenData ScreenName = " + ScreenName + ",Segment = " + Segment);
            Screencustomization screendata = new Screencustomization();
            try
            {
                screendata = this.screencustomizationDAL.GetScreenData(ScreenName, Segment);
                logger.Info("ScreenData Retrieved = " + screendata.ToString());
            }
            catch (Exception e)
            {
                logger.Error("Error Occurred in GetScreenData " + e.Message);
            }
            return screendata;
        }
    }
}