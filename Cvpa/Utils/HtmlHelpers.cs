﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cvpa.Utils
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString BootstrapPager(this HtmlHelper helper, long pageNumber, Func<long, string> action, long totalItems, long pageSize = 10, long numberOfLinks = 5, long itemsCount = 0)
        {

            if (totalItems <= 0)
            {
                return MvcHtmlString.Empty;
            }

            var totalPages = (long)Math.Ceiling(totalItems / (double)pageSize);
            var lastPageNumber = (long)Math.Ceiling((double)pageNumber / numberOfLinks) * numberOfLinks;
            var firstPageNumber = lastPageNumber - (numberOfLinks - 1);
            var hasPreviousPage = pageNumber > 1;
            var hasNextPage = pageNumber < totalPages;
            if (lastPageNumber > totalPages)
            {
                lastPageNumber = totalPages;
            }
            var ul = new TagBuilder("ul");
            ul.AddCssClass("pagination");
            ul.InnerHtml += AddLink(1, action, pageNumber == 1, "disabled", "<<", "First Page");
            ul.InnerHtml += AddLink(pageNumber - 1, action, !hasPreviousPage, "disabled", "<", "Previous Page");
            if (totalPages > 10) // show pages as select box if totalPages > 10
            {
                var selectPage = new TagBuilder("select");
                selectPage.Attributes.Add("id", "paginationSelect");
                selectPage.Attributes.Add("style", "padding: 7px; border: none;");
                selectPage.Attributes.Add("onChange", "document.location.href=this.value");
                var liItem = new TagBuilder("li");
                for (long i = firstPageNumber; i <= lastPageNumber; i++)
                {
                    var optionItem = new TagBuilder("option");
                    optionItem.Attributes.Add("value", action(i));
                    if (pageNumber == i)
                        optionItem.Attributes.Add("selected", "selected");
                    optionItem.SetInnerText("" + i + " Page");
                    selectPage.InnerHtml += optionItem;
                    //ul.InnerHtml += AddLink(i, action, i == pageNumber, "active", i.ToString(), i.ToString());
                }
                liItem.InnerHtml = selectPage.ToString();
                ul.InnerHtml += liItem.ToString();
            }
            else // Show page links if totalPages is less than or equal to 10
            {
                for (long i = firstPageNumber; i <= lastPageNumber; i++)
                {
                    ul.InnerHtml += AddLink(i, action, i == pageNumber, "active", i.ToString(), i.ToString());
                }
            }
            ul.InnerHtml += AddLink(pageNumber + 1, action, !hasNextPage, "disabled", ">", "Next Page");
            ul.InnerHtml += AddLink(totalPages, action, pageNumber == totalPages, "disabled", ">>", "Last Page");
            return MvcHtmlString.Create(ul.ToString());
        }

        private static TagBuilder AddLink(long index, Func<long, string> action, bool condition, string classToAdd, string linkText, string tooltip)
        {
            var li = new TagBuilder("li");
            li.MergeAttribute("title", tooltip);
            if (condition)
            {
                li.AddCssClass(classToAdd);
            }
            var a = new TagBuilder("a");
            a.MergeAttribute("href", !condition ? action(index) : "javascript:");
            a.SetInnerText(linkText);
            li.InnerHtml = a.ToString();
            return li;
        }


    }
}