﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cvpa.Utils
{
    public class Auth
    {
        public static void SetStatusMessage(string Message, string StatusType = null)
        {
            HttpContext.Current.Session["Message"] = Message;
            HttpContext.Current.Session["MessageStatus"] = StatusType;
        }

        public static string GetStatusMessage()
        {
            return (HttpContext.Current.Session["Message"] ?? "").ToString();
        }

        public static string GetMessageStatusType()
        {
            return (HttpContext.Current.Session["MessageStatus"] ?? "").ToString();
        }

        public static void EmptyStatusMessage()
        {
            SetStatusMessage("");
        }
    }
}