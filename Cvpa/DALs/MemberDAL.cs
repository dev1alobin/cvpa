﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AloFramework.Core.DALs;
using Cvpa.Models;
using PetaPoco;

namespace Cvpa.DALs
{
    public class MemberDAL : AbstractDAL<Member>
    {
        public IEnumerable<Member> All()
        {
            return this.dbContext.Query<Member>("select * from Members where Stauts =@0", "1");
            //return this.dbContext.Page<Member>(page, itemsPerPage, "select * from Members", new { });            
        }

        public void Save(Member member)
        {
            this.dbContext.Save(member);
        }
        public void Update(Member member)
        {
            this.dbContext.Update(member);

        }

        public int Delete(long ID)
        {
            int result = 0;
            Member member = this.FindByID(ID);

            if (member != null)
            {
                member.Status = 0;
                this.dbContext.Update(member);
                result = 1;
            }
            return result;
        }
        public int Dismiss(long ID, string Reason)
        {
            int result = 0;
            Member member = this.FindByID(ID);

            if (member != null)
            {
                member.Status = 2;
                member.DismissDate = DateTime.Now.Date;
                member.DismissReason = Reason;
                this.dbContext.Update(member);
                result = 1;
            }
            return result;
        }

        public Member FindByID(long ID)
        {
            return this.dbContext.First<Member>("select Members.* from members where MemberID = @0", ID);
        }

        public Member FindByMemberNo(long ID)
        {
            return this.dbContext.First<Member>(" where MemberNo = @0", ID);
        }

        public List<Member> GetMembershipNo(long ID)
        {
            return this.dbContext.Fetch<Member>("where (MemberNo like @0 or  MobileNo like @1) and Status = @2", "%" + ID, "%" + ID, "1");
        }

        public List<Member> GetMemberNoForMeeting(string ColumnName, long term)
        {
            return this.dbContext.Fetch<Member>("select  * from Members where (" + ColumnName + " like @0) and Status = @1", "%" + term + "%", "1");

        }

        public int GetLastMemberID()
        {
            return this.dbContext.First<int>(" SELECT MAX(MemberNo) FROM Members where Status = @0", "1");
        }

        public List<Member> MembersPrint(Sql query)
        {
            return this.dbContext.Fetch<Member>(query);
        }

        public int GetMembersCount()
        {
            return this.dbContext.First<int>("select Count(MemberNo) from Members  where Status = @0", "1");
        }

        public int GetExpiredMembersCount()
        {
            return this.dbContext.First<int>("select Count(MemberNo) from Members  where Status = @0 and (MembershipExpiryDate < @1 OR MembershipExpiryDate is null)", "1", DateTime.Now.Date);
        }

        public int GetLifeMemberCount()
        {
            return this.dbContext.First<int>("select Count(MemberNo) from Members  where Status = @0 and MemberType = @1", "1", "Life Member");
        }

        public int GetActiveMembersCount()
        {
            return this.dbContext.First<int>("select Count(MemberNo) from Members  where Status = @0 and ((NOW() BETWEEN MembershipStartDate AND MembershipExpiryDate)) ", "1");
        }

        public List<Member> GetMembersExpiry()
        {
            return this.dbContext.Fetch<Member>("where Status = @0", "1");
        }

        public List<string> GetMemberAutoComplete(string ColumnName, string term)
        {
            return this.dbContext.Fetch<string>("select distinct " + ColumnName + "  from Members where " + ColumnName + " like @0 and Status = @1", term + "%", "1");
        }

        //  Check whether MemberNo avaliable in members table for New Member Inserting through Receipt.  
        public Member GetMemberForReceiptInsert(long memberID)
        {
            return this.dbContext.First<Member>("where MemberID =@0", memberID);
        }

        public List<Member> SendSms(string SmsTo, string Area)
        {
            if (SmsTo == "All")
            {
                return this.dbContext.Fetch<Member>("select MemberNo,MemberName,MobileNo,Religion from Members");
            }
            else if (!string.IsNullOrEmpty(Area) && SmsTo == "Areawise")
            {
                return this.dbContext.Fetch<Member>("select MemberNo,MemberName,MobileNo from Members where StoreArea =@0", Area);
            }
            else if (!string.IsNullOrEmpty(Area) && SmsTo == "Zonewise")
            {
                return this.dbContext.Fetch<Member>("select MemberNo,MemberName,MobileNo from Members where Zone =@0", Area);
            }
            else if (SmsTo != "MobileNumber")
            {
                return this.dbContext.Fetch<Member>("select MemberNo,MemberName,MobileNo,Religion from Members where Religion =@0", SmsTo);
            }
            else
            {
                return null;
            }
        }

        public List<string> MembersAreaList(string term)
        {
            return this.dbContext.Fetch<string>("select distinct StoreArea  from Members where StoreArea like @0 and Status = @1", "%" + term + "%", "1");
        }

        public List<string> MembersAreaListByArea(string term, string areaType)
        {
            if (!String.IsNullOrEmpty(areaType) && areaType.Equals("Zonewise"))
            {
                return this.dbContext.Fetch<string>("select distinct Zone from Members where Zone like @0 and Status = @1", "%" + term + "%", "1");
            }
            else
            {
                return this.dbContext.Fetch<string>("select distinct StoreArea  from Members where StoreArea like @0 and Status = @1", "%" + term + "%", "1");
            }
        }

        public List<Member> GetMembersForInsurence()
        {
            return this.dbContext.Fetch<Member>("Select * from members where MembershipStartDate is not null and MembershipExpiryDate is not null and Status = @0", 1);
        }

        public List<string> GetZoneList()
        {
            return this.dbContext.Fetch<string>("Select Distinct Zone from Members where Status = @0 and Zone is not null", "1");
        }

        public List<string> GetAreaList(string ZoneName)
        {
            if (ZoneName != "" && ZoneName != null)
            {
                return this.dbContext.Fetch<string>("Select Distinct StoreArea from Members where Status = @0 and Zone = @1 and StoreArea is not null", "1", ZoneName);
            }
            else {
                return this.dbContext.Fetch<string>("Select Distinct StoreArea from Members where Status = @0 and StoreArea is not null", "1");
            }
        }
    }
}