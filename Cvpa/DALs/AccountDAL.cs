﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AloFramework.Core.DALs;
using Cvpa.Models;
using PetaPoco;

namespace Cvpa.DALs
{
    public class AccountDAL : AbstractDAL<User>
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(AccountDAL));
        public User AuthenticateUser(User user) {
            return this.dbContext.First<User>(" where Username=@0 and Password=@1",user.Username, user.Password);
        }

        public void Save(Account Account)
        {
            this.dbContext.Save(Account);
        }
        public void Update(Account Account)
        {
            logger.Info("Receipt ID in Update = "+Account.ReceiptID);
            //Account.Status = 1;
            this.dbContext.Update("Accounts", "AccountID", Account);
        }

        public void Delete(Account account)
        {
            //account.Status = 0;
            this.dbContext.Update<Account>("SET Status = @0 where AccountID=@1",0,account.AccountID);
        }
        public Account FindByID(long ID)
        {
            var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       Where AccountID = @0", ID);
            return this.dbContext.First<Account>(query);
        }

        public Account FindByIncomeBillNo(long ID)
        {
            var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       Where IncomeBillNo = @0", ID);
            return this.dbContext.First<Account>(query);
        }

        public Account FindByExpenseBillNo(long ID)
        {
            var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       Where ExpenseBillNo = @0", ID);
            return this.dbContext.First<Account>(query);
        }

        public List<Account> AccountsPrint(Sql query)
        {
            return this.dbContext.Fetch<Account>(query);
        }

        public List<Account> IncomeReceiptPrint(string Query, string FromDate,  string ToDate)
        {
            if (!string.IsNullOrEmpty(Query))
            {
                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                        Where  (ExpenseBillNo like @0 OR Name like @0 or ReceiptDate like @0 or Amount like @0 or ChequeDate like @0 or ChequeNo like @0 OR Categorys.CategoryName like @0 or Reason like @0 or MobileNo like @0) 
                                        AND PaymentType = @1  AND Accounts.Status = @2 and Categorys.CategoryType Not In(@3) order BY ReceiptDate Desc"
                                       , "%" + Query + "%"
                                       , "Credit"
                                       , "1"
                                       , "Receipt");
                return this.dbContext.Fetch<Account>(query);
            }
            else if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
            {
                DateTime MyFromDate = DateTime.ParseExact(FromDate, "dd/MM/yyyy", null);
                DateTime MyToDate = DateTime.ParseExact(ToDate, "dd/MM/yyyy", null);

                string FromDateStr = MyFromDate.ToString("yyyy-MM-dd");
                string ToDateStr = MyToDate.ToString("yyyy-MM-dd");

                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                        where Accounts.Status = @0 and PaymentType = @1 and Categorys.CategoryType Not In(@2) and ReceiptDate between @3 and @4"
                   , "1"
                   , "Credit"
                   , "Receipt"
                   , FromDateStr
                   , ToDateStr);

                return this.dbContext.Fetch<Account>(query);
            }
            else if (!string.IsNullOrEmpty(FromDate))
            {
                DateTime MyFromDate = DateTime.ParseExact(FromDate, "dd/MM/yyyy", null);

                string FromDateStr = MyFromDate.ToString("yyyy-MM-dd");

                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                        where Accounts.Status = @0 and PaymentType = @1 and Categorys.CategoryType Not In(@2) and ReceiptDate >= @3"
                   , "1"
                   , "Credit"
                   , "Receipt"
                   , FromDateStr);

                return this.dbContext.Fetch<Account>(query);
            }
            else if (!string.IsNullOrEmpty(ToDate))
            {
                DateTime MyToDate = DateTime.ParseExact(ToDate, "dd/MM/yyyy", null);
                
                string ToDateStr = MyToDate.ToString("yyyy-MM-dd");

                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                        where Accounts.Status = @0 and PaymentType = @1 and Categorys.CategoryType Not In(@2) and ReceiptDate <= @3"
                   , "1"
                   , "Credit"
                   , "Receipt"
                   , ToDateStr);

                return this.dbContext.Fetch<Account>(query);
            }
            else
            {
                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       Where Accounts.Status = @0 and PaymentType=@1 and Categorys.CategoryType Not In(@2) ORDER BY ReceiptDate DESC", "1", "Credit", "Receipt");
                return this.dbContext.Fetch<Account>(query);
            }
        }

        public List<Account> ExpenseReceiptPrint(string Query, string FromDate, string ToDate)
        {
            if (!string.IsNullOrEmpty(Query))
            {
                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                        Where  (ExpenseBillNo like @0 OR Name like @0 or ReceiptDate like @0 or Amount like @0 or ChequeDate like @0 or ChequeNo like @0 OR Categorys.CategoryName like @0 or Reason like @0 or MobileNo like @0) 
                                        AND PaymentType = @1  AND Accounts.Status = @2 order BY ReceiptDate Desc"
                                       , "%" + Query + "%"
                                       , "Debit"
                                       , "1");
                return this.dbContext.Fetch<Account>(query);
            }
            else if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
            {
                DateTime MyFromDate = DateTime.ParseExact(FromDate, "dd/MM/yyyy", null);
                DateTime MyToDate = DateTime.ParseExact(ToDate, "dd/MM/yyyy", null);

                string FromDateStr = MyFromDate.ToString("yyyy-MM-dd");
                string ToDateStr = MyToDate.ToString("yyyy-MM-dd");

                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       where Accounts.Status = @0 and PaymentType = @1 and ReceiptDate between @2 and @3"
                                       , "1"
                                       , "Debit"
                                       , FromDateStr
                                       , ToDateStr);
                return this.dbContext.Fetch<Account>(query);
            }
            else if (!string.IsNullOrEmpty(FromDate))
            {
                DateTime MyFromDate = DateTime.ParseExact(FromDate, "dd/MM/yyyy", null);

                string FromDateStr = MyFromDate.ToString("yyyy-MM-dd");

                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       where Accounts.Status = @0 and PaymentType = @1 and ReceiptDate >= @2"
                                       , "1"
                                       , "Debit"
                                       , FromDateStr);
                return this.dbContext.Fetch<Account>(query);
            }
            else if (!string.IsNullOrEmpty(FromDate))
            {
                DateTime MyToDate = DateTime.ParseExact(ToDate, "dd/MM/yyyy", null);
                
                string ToDateStr = MyToDate.ToString("yyyy-MM-dd");

                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       where Accounts.Status = @0 and PaymentType = @1 and ReceiptDate <= @2"
                                       , "1"
                                       , "Debit"
                                       , ToDateStr);
                return this.dbContext.Fetch<Account>(query);
            }
            else
            {
                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       Where Accounts.Status = @0 and PaymentType=@1 ORDER BY ReceiptDate DESC", "1", "Debit");
                return this.dbContext.Fetch<Account>(query);
            }
        }

        public int GetLastReceiptNo()
        {
            //return this.dbContext.First<int>(" SELECT MAX(AccountNo) FROM Accounts");
            return this.dbContext.First<int>("SELECT IFNULL(MAX(AccountNo), 0) FROM Accounts");
        }

        public List<Account> GetIncomeAndExpenseDetails(DateTime FromDate, DateTime ToDate)
        {
            if (FromDate != null)
            {
                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       where Accounts.Status = @0 and ReceiptDate between @2 and @3"
                                      , "1"
                                      , FromDate
                                      , ToDate);
                return this.dbContext.Fetch<Account>(query);
            }
            else {
                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                        where Accounts.Status =@0 and ReceiptDate =@1", "1", DateTime.Now);
                return this.dbContext.Fetch<Account>(query);
            }
        }


        public List<Account> IncomeAndExpenseDetailsPrint(string FromDate, string ToDate)
        {
            if (!String.IsNullOrEmpty(FromDate) && !String.IsNullOrEmpty(ToDate))
            {
                DateTime MyFromDate = DateTime.ParseExact(FromDate, "dd/MM/yyyy", null);
                DateTime MyToDate = DateTime.ParseExact(ToDate, "dd/MM/yyyy", null);

                string NewFromDate = MyFromDate.ToString("yyyy-MM-dd");
                string NewToDate = MyToDate.ToString("yyyy-MM-dd");

                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       where Accounts.Status = @0 and ReceiptDate between @1 and @2 order by ReceiptDate DESC"
                                      , "1"
                                      , NewFromDate
                                      , NewToDate);
                return this.dbContext.Fetch<Account>(query);
            }
            else if (!String.IsNullOrEmpty(FromDate))
            {
                DateTime MyFromDate = DateTime.ParseExact(FromDate, "dd/MM/yyyy", null);

                string NewFromDate = MyFromDate.ToString("yyyy-MM-dd");

                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       where Accounts.Status = @0 and ReceiptDate >= @1 order by ReceiptDate DESC"
                                      , "1"
                                      , NewFromDate);
                return this.dbContext.Fetch<Account>(query);
            }
            else if (!String.IsNullOrEmpty(ToDate))
            {
                DateTime MyToDate = DateTime.ParseExact(ToDate, "dd/MM/yyyy", null);
                
                string NewToDate = MyToDate.ToString("yyyy-MM-dd");

                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                       where Accounts.Status = @0 and ReceiptDate <= @1 order by ReceiptDate DESC"
                                      , "1"
                                      , NewToDate);
                return this.dbContext.Fetch<Account>(query);
            }
            else
            {
                var query = new Sql(@"Select Accounts.*, Categorys.CategoryName from Accounts 
                                        Left Join Categorys On Categorys.CategoryID = Accounts.CategoryID
                                        where Accounts.Status =@0 and Accounts.ReceiptDate =@1 order by ReceiptDate DESC", "1", DateTime.Now.Date);
                return this.dbContext.Fetch<Account>(query);
            }
        }

        public List<Account> GetGroupByIncomeAndExpenseDetailsPrint(string FromDate, string ToDate)
        {
            if (FromDate != null)
            {
                var query = new Sql(@"SELECT Sum(Amount) as Amount,Accounts.CategoryID,Accounts.ReceiptDate,Accounts.Reason,Categorys.CategoryName,PaymentType FROM `accounts`
                                        Left Join Categorys on Categorys.CategoryID = Accounts.CategoryID Where Accounts.Status = @0 and (ReceiptDate between @1 and @2) GROUP BY Accounts.CategoryID"
                                      , "1"
                                      , FromDate
                                      , ToDate);
                return this.dbContext.Fetch<Account>(query);
            }
            else
            {
                var query = new Sql(@"SELECT Sum(Amount) as Amount,Accounts.CategoryID,Accounts.ReceiptDate,Accounts.Reason,Categorys.CategoryName,PaymentType FROM `accounts`
                                        Left Join Categorys on Categorys.CategoryID = Accounts.CategoryID Where Accounts.Status = @0 and (ReceiptDate = @1) GROUP BY Accounts.CategoryID", "1", DateTime.Now);
                return this.dbContext.Fetch<Account>(query);
            }
        }


    }
}