﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cvpa.Models;
using PetaPoco;
using AloFramework.Core.DALs;

namespace Cvpa.DALs
{
    public class ScreencustomizationDAL : AbstractDAL<Screencustomization>
    {
        public Screencustomization GetScreenData(string ScreenName, string Segment) {
            return this.dbContext.First<Screencustomization>("Where ScreenName=@0 and Segment=@1", ScreenName, Segment);
        }
    }
}