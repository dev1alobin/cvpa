﻿using AloFramework.Core.DALs;
using Cvpa.Controllers;
using Cvpa.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cvpa.DALs
{
    public class MeetingDAL : AbstractDAL<Meeting>
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(MeetingController));
        public void Save(Meeting meeting)
        {
            try
            {
                this.dbContext.Save(meeting);
            }
            catch (Exception e)
            {
                logger.Info("Error Message in Meetin DAL" + e.Message);
            }
        }
        /* public void Update(Meeting meeting)
         {
             try
             {
                 this.dbContext.Update(meeting);
             }
             catch(Exception e)
             {
                 logger.Info("Error Message in Meetin DAL" + e);
             }
         }*/

        public bool SaveMemberEntry(MemberEntry memberEntry)
        {

            try
            {
                if (memberEntry.MemberEntryID > 0)
                {

                    this.dbContext.Update("MemberEntries", "MemberEntryID", memberEntry);
                    //this.dbContext.Update(product);
                }
                else
                {

                    this.dbContext.Insert("MemberEntries", "MemberEntryID", memberEntry);
                    //this.dbContext.Insert(product);
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Info("error Message in Product DAL" + e.Message);
                return false;
            }
        }



        public bool getMemberExists(MemberEntry memberEntry)
        {
            var member = this.dbContext.Fetch<MemberEntry>("Select * From MemberEntries where  MemberID = @0 AND MeetingID = @1", memberEntry.MemberID, memberEntry.MeetingID);
            if (member.Count > 0)
            {
                return true;
            }
            return false;
        }

        public Meeting FindByID(long ID)
        {
            return this.dbContext.First<Meeting>("where MeetingID = @0", ID);
        }

        public int Delete(long ID)
        {
            int result = 0;
            Meeting meeting = this.FindByID(ID);

            if (meeting != null)
            {
                meeting.Status = 0;
                this.dbContext.Delete(meeting);
                result = 1;
            }
            return result;
        }
        public List<MemberEntry> MembersPrint(string MeetingID)
        {

            var memberlist = this.dbContext.Fetch<MemberEntry>(@"SELECT meetings.MeetingID,meetings.MeetingTitle,meetings.MeetingPlace,meetings.MeetingDate,memberentries.MemberNo
                                                               ,memberentries.MemberName,memberentries.PhoneNo,memberentries.ShopName FROM meetings
                                                               LEFT JOIN memberentries ON meetings.MeetingID = memberentries.MeetingID
                                                               where meetings.MeetingID like @0", "%" + MeetingID + "%");
            return memberlist;
        }
        /* public Meeting GetMeeting(long meetingID)
        {
            return this.dbContext.FetchO
        }*/
    }

}