﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AloFramework.Core.DALs;
using Cvpa.Models;
using Cvpa.Utils;
using Cvpa.BALs;
using PetaPoco;

namespace Cvpa.DALs
{
    public class InsuranceDAL : AbstractDAL<Insurance>
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(InsuranceDAL));
        private MemberBAL memberBAL = new MemberBAL();
        public IEnumerable<Insurance> All()
        {
            return this.dbContext.Query<Insurance>("select * from Insurances  where Stauts =@0","1");
        }

        public void Save(Insurance insurance)
        {
            try
            {
                this.dbContext.Save(insurance);
               
                Member MemberDetails = new Member();
                try
                {
                    //logger.Info("Inside GetMemberForInsuranceInsert MembershipNO = " + insurance.MembershipNo);
                    //MemberDetails = this.memberBAL.GetMemberForReceiptInsert(insurance.MembershipNo);
                    //logger.Info("Checking whether member is available in members table MembershipID = " + MemberDetails.MembershipNo);
                    //if (MemberDetails.MembershipNo > 0)
                    //{

                    //    this.dbContext.Update<Member>("SET InsuranceAmount=@0 , InsurancePaidDate=@1 where MembershipNo=@2 ", insurance.PaidAmount, insurance.ReceiptDate, insurance.MembershipNo);
                    //}
                    ///*else
                    //{
                    //    logger.Info("Inside SaveMember in Receipts");
                    //    this.dbContext.Execute("Insert into Members (MembershipNo,MemberName,PhoneNo,StoreName,MemberShipAmountPaidDate,MembershipExpiryDate,MembershipAmount,MembershipForYear,Status) values(@0,@1,@2,@3,@4,@5,@6,@7,@8)", receipt.MembershipNo, receipt.MemberName, receipt.PhoneNo, receipt.StoreName, receipt.ReceiptDate, expirydate, receipt.PaidAmount, receipt.ReceiptFromYear + "-" + receipt.ReceiptToYear, "1");  //Insert("Members","MembershipNo = @0",receipt.MembershipNo);
                    //}*/
                }
                catch(Exception e) {
                    logger.Info("Error in GetMemberForInsurance" + e.Message);
                }
                
            }
            catch (Exception e) { 
                logger.Info("Error in Save Receipt " +e.Message);
            }

        }
        public void Update(Insurance insurance)
        {
            logger.Info("Update Insurance");

            this.dbContext.Update(insurance);
        //    this.dbContext.Update<Member>("SET InsuranceAmount=@0 , InsurancePaidDate=@1 where MembershipNo=@2 ", insurance.PaidAmount, insurance.ReceiptDate, insurance.MembershipNo);
        }

        public int GetLastInsuranceNo() 
        {
            return this.dbContext.First<int>(" SELECT MAX(InsuranceNo) FROM Insurances");
        }

        public void Delete(Insurance insurance)
        {
            insurance.Status = 0;
            this.dbContext.Update(insurance);
        }

        public Insurance FindByID(long insuranceID)
        {
            return this.dbContext.First<Insurance>(@"Select Insurances.*, Members.MemberNo, Members.MemberName, Members.MobileNo, Members.NomineeName, Members.Relationship from Insurances
                                                    Left Join Members on Members.MemberID = Insurances.MemberID  where InsuranceID = @0", insuranceID);
        }



        public List<Insurance> InsuranceReceiptListPrint(Sql query)
        {
            return this.dbContext.Fetch<Insurance>(query);
        }

        // Dashboard Insurance Count
        public List<Insurance> GetInsurancesCount()
        {
            var Date = DateTime.Today;
            return this.dbContext.Fetch<Insurance>("where Status = @0 and ReceiptDate = @1", "1", Date);
            
        }

        // Dashboard Print Option
        public Insurance InsuranceReceiptSinglePrint(long InsuranceID, long MemberID)
        {
            Insurance insurance = new Insurance();
            if (InsuranceID > 0)
            {
                var query = new Sql(@"Select Insurances.*, Members.MemberNo,Members.MemberName,Members.MobileNo, Members.NomineeName, Members.Relationship from Insurances
                                     Left Join Members On Members.MemberID = Insurances.MemberID
                                     where  InsuranceID = @0", InsuranceID);
                insurance = this.dbContext.Fetch<Insurance>(query).FirstOrDefault();
            }
            else if (MemberID > 0)
            {
    
                var CurrentYear = DateTime.Now.Year.ToString();
                //CurrentYear + 1;
                logger.Info("CurrentYear = " + (CurrentYear));
                insurance = this.dbContext.First<Insurance>(" where MemberNo like @0 ", MemberID);
            }
            return insurance;
        }

        public void InsertInsurance(List<Member> members)
        {
            try
            {
                foreach (var member in members)
                {
                    var insurance = new Insurance();
                    insurance.MemberID = member.MemberID;
                    insurance.ReceiptFromYear = member.MembershipStartDate.Value;
                    insurance.ReceiptToYear = member.MembershipExpiryDate.Value;
                    insurance.ReceiptDate = new DateTime(insurance.ReceiptFromYear.Year, insurance.ReceiptFromYear.Month, insurance.ReceiptFromYear.Day);
                    insurance.Status = 1;
                    this.dbContext.Save(insurance);
                }
            }
            catch (Exception ex)
            {
                logger.Error("Problem in inserting Insurance, due to " + ex);
            }
        }

    }
}