﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AloFramework.Core.DALs;
using Cvpa.Models;
using Cvpa.Utils;
using Cvpa.BALs;
using System.Globalization;
using PetaPoco;

namespace Cvpa.DALs
{
    public class ReceiptDAL : AbstractDAL<Receipt>
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(ReceiptDAL));
        private MemberBAL memberBAL = new MemberBAL();
        private MemberDAL memberDAL = new MemberDAL();
        private AccountDAL accountDAL = new AccountDAL();
        private InsuranceDAL insuranceDAL = new InsuranceDAL();
        public IEnumerable<Receipt> All()
        {
            return this.dbContext.Query<Receipt>("select * from Receipts  where Stauts =@0", "1");
        }

        public void Save(Receipt receipt)
        {
            try
            {
                this.dbContext.Save(receipt);


                Member MemberDetails = new Member();
                try
                {
                    logger.Info("Inside GetMemberForReceiptInsert MembershipNO = " + receipt.MemberID);
                    MemberDetails = this.memberBAL.GetMemberForReceiptInsert(receipt.MemberID);
                    logger.Info("Checking whether member is available in members table MembershipID = " + MemberDetails.MemberNo);

                    // Getting From and To Year From Receipt
                    DateTime ReceiptFromYear = receipt.ReceiptFromYear;
                    DateTime ReceiptToYear = receipt.ReceiptToYear;
                    var fromYear = ReceiptFromYear.Year;
                    var toYear = ReceiptToYear.Year;
                    logger.Info("Receipt from and To Year = " + fromYear + " - " + toYear);

                    // Getting MemberNo from Members Table 
                    long LastMemberID;
                    LastMemberID = this.memberDAL.GetLastMemberID();
                    logger.Info("LastMemberID =" + LastMemberID);
                    var memberNo = LastMemberID + 1;
                    var MemberType = "";
                    if (receipt.CategoryID == 3)
                    {
                        MemberType = "LIFE MEMBER";
                    }
                    else
                    {
                        MemberType = "MEMBER";
                    }

                    if (MemberDetails.MemberNo > 0)
                    {
                        this.dbContext.Update<Member>("SET MembershipAmount=@0 ,CategoryID=@1, MembershipExpiryDate=@2, MembershipStartDate=@7, MembershipForYear=@3, MembershipPaid=@4, MemberType=@5 where MemberID=@6"
                            , receipt.PaidAmount
                            , receipt.CategoryID
                            , receipt.ReceiptToYear
                            , fromYear + " - " + toYear
                            , "Paid"
                            , MemberType
                            , receipt.MemberID
                            , receipt.ReceiptFromYear);
                    }
                    else
                    {
                        logger.Info("Inside SaveMember in Receipts");

                        this.dbContext.Execute("Insert into Members (MemberNo,MemberName,CategoryID,MobileNo,StoreName,StoreAddress,StoreArea,StoreCity,StorePincode,MembershipExpiryDate,MembershipStartDate,MembershipAmount,MembershipForYear,MembershipPaid,MemberType,Status) values(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15)"
                            , memberNo
                            , receipt.MemberName
                            , receipt.CategoryID
                            , receipt.MobileNo
                            , receipt.StoreName
                            , receipt.StoreAddress
                            , receipt.StoreArea
                            , receipt.StoreCity
                            , receipt.StorePincode
                            , receipt.ReceiptToYear
                            , receipt.ReceiptFromYear
                            , receipt.PaidAmount
                            , fromYear + " - " + toYear
                            , "Paid"
                            , MemberType
                            , "1");

                        //Inserting MemberID in receipt table
                        var memberID = this.dbContext.First<long>("Select MemberID from Members Where MemberNo = @0", memberNo);
                        receipt.MemberID = memberID;
                        this.dbContext.Update(receipt);
                    }

                    //Inserting Receipt Amount In Accounts Table

                    if (receipt.ReceiptID > 0)
                    {

                        int lastreceiptno = 0;
                        lastreceiptno = this.accountDAL.GetLastReceiptNo() + 1;
                        this.dbContext.Execute("Insert into Accounts (AccountNo,ReceiptID,ReceiptBillNo,Name,Amount,AmountInWords,Reason,PaymentMode,PaymentType,CategoryID,ChequeNo,ChequeDate,ReceiptDate,Status,MobileNo) values(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14)"
                           , lastreceiptno
                           , receipt.ReceiptID
                           , receipt.ReceiptBillNo
                           , receipt.MemberName
                           , receipt.PaidAmount
                           , receipt.AmountInWords
                           , "MembershipFees"
                           , receipt.PaymentMode
                           , "Credit"
                           , receipt.CategoryID
                           , null
                           , null
                           , receipt.ReceiptDate
                           , "1"
                           , receipt.MobileNo);
                    }

                    // Inserting Insurence Details 
                    this.UpdateInsurance(receipt);
                    // int getlastinsuranceno = 0;


                }
                catch (Exception e)
                {
                    logger.Info("Error in GetMemberForReceipt" + e.Message);
                }

            }
            catch (Exception e)
            {
                logger.Info("Error in Save Receipt " + e.Message);
            }

        }

        public void UpdateInsurance(Receipt receipt)
        {
            try
            {
                //  getlastinsuranceno = this.insuranceDAL.GetLastInsuranceNo();
                //  getlastinsuranceno = getlastinsuranceno + 1;
                var insurance = this.dbContext.Fetch<Insurance>("Select * from Insurances where MemberID = @0", receipt.MemberID).FirstOrDefault();
                if (insurance == null)
                {
                    insurance = new Insurance();
                    insurance.InsuranceID = 0;
                    insurance.MemberID = receipt.MemberID;
                    insurance.ReceiptDate = DateTime.Now.Date;
                    insurance.Status = 1;
                }
                insurance.ReceiptFromYear = receipt.ReceiptFromYear;
                insurance.ReceiptToYear = receipt.ReceiptToYear;
                /*this.dbContext.Execute("Insert into Insurances (ReceiptDate,MemberID,Status,ReceiptID,ReceiptFromYear,ReceiptToYear) values(@0,@1,@2,@3,@4,@5)"
                        , receipt.ReceiptDate
                        , receipt.MemberID
                        , "1"
                        , receipt.ReceiptID
                        ,receipt.ReceiptFromYear
                        ,receipt.ReceiptToYear);*/
                this.dbContext.Save(insurance);
                // Updating InsuranceNo in Receipt Table
            }
            catch (Exception e)
            {
                logger.Info("Problem in updating Insurance, while saving Receipt[ReceiptID=" + receipt.ReceiptID + "], due to " + e.Message);
            }
        }

        public void Update(Receipt receipt)
        {
            logger.Info("Update Receipt");

            DateTime ReceiptFromYear = receipt.ReceiptFromYear;
            DateTime ReceiptToYear = receipt.ReceiptToYear;
            var fromYear = ReceiptFromYear.Year;
            var toYear = ReceiptToYear.Year;
            logger.Info("Receipt from and To Year = " + fromYear + " - " + toYear);
            this.dbContext.Update(receipt);
            this.dbContext.Update<Member>("SET MembershipAmount=@0 ,CategoryID=@1, MembershipExpiryDate=@2, MembershipForYear=@3, MembershipStartDate=@5 where MemberID=@4 ", receipt.PaidAmount, receipt.CategoryID, receipt.ReceiptToYear, fromYear + " - " + toYear, receipt.MemberID, receipt.ReceiptFromYear);
            this.dbContext.Update<Account>("SET Amount =@0, AmountInWords =@1, ReceiptDate = @2, ReceiptBillNo = @3, CategoryID = @4, Status = @5 where ReceiptID = @6", receipt.PaidAmount, receipt.AmountInWords, receipt.ReceiptDate, receipt.ReceiptBillNo, receipt.CategoryID, "1", receipt.ReceiptID);

            this.UpdateInsurance(receipt);
        }

        public int GetLastReceiptNo()
        {
            return this.dbContext.First<int>(" SELECT MAX(ReceiptNo) FROM Receipts");
        }

        public void Delete(Receipt receipt)
        {
            //Deleting the Receipt 
            receipt.Status = 0;
            this.dbContext.Update(receipt);

            this.dbContext.Update<Account>("Set Status = @0 Where ReceiptID = @1", 0, receipt.ReceiptID);
            this.dbContext.Update<Member>("Set MembershipAmount=@0,MembershipExpiryDate=@1,MembershipStartDate=@6,CategoryID=@2,MembershipForYear=@3, MembershipPaid = @4 where MemberID=@5", 0, null, 0, "NULL", "Not Paid", receipt.MemberID, null);
            this.dbContext.Update<Insurance>("Set Status = @0 Where ReceiptID = @1", 0, receipt.ReceiptID);
            //Deleting the Insurance
            //this.dbContext.Update<Insurance>("SET Status=@0 WHERE ReceiptNo=@1"
            //, "0"
            //, receipt.ReceiptNo);
        }

        public Receipt FindByID(long receiptID)
        {
            var query = new Sql(@"Select receipts.*,
                                members.MemberNo,members.MemberName, members.StoreAddress, members.StoreArea, members.StoreCity, members.StorePincode, members.MobileNo, members.StoreName from Receipts
                                Left Join members On members.MemberID = Receipts.MemberID
                                Where receipts.ReceiptID like @0", receiptID + "%");
            return this.dbContext.First<Receipt>(query);
        }


        public List<Receipt> ReceiptPrint(Sql query)
        {
            return this.dbContext.Fetch<Receipt>(query);
        }

        // Dashboard Receipt Count
        public List<Receipt> GetReceiptsCount()
        {
            var Date = DateTime.Today;
            //return this.dbContext.Fetch<Receipt>("where Status = @0 and ReceiptDate = @1", "1", Date);
            return this.dbContext.Fetch<Receipt>("SELECT PaidAmount FROM RECEIPTS where Status = @0 and MONTH(ReceiptDate) = MONTH(NOW())", "1");
        }

        // Dashboard Print Option
        public Receipt ReceiptSinglePrint(long ReceiptID, long MemberID)
        {
            Receipt receipt = new Receipt();
            if (ReceiptID > 0)
            {
                var query = new Sql(@"Select receipts.*,
                                members.MemberNo,members.MemberName, members.StoreAddress, members.StoreArea, members.StoreCity, members.StorePincode, members.MobileNo, members.StoreName from Receipts
                                Left Join members On members.MemberID = Receipts.MemberID
                               where ReceiptID like @0", ReceiptID);
                receipt = this.dbContext.First<Receipt>(query);
            }
            else if (MemberID > 0)
            {

                var query = new Sql(@"Select receipts.*,
                                members.MemberNo,members.MemberName, members.StoreAddress, members.StoreArea, members.StoreCity, members.StorePincode, members.MobileNo, members.StoreName from Receipts
                                Left Join members On members.MemberID = Receipts.MemberID
                                where ReceiptBillNo like @0", MemberID);  // MemberID contains BillNo This is done for reports menu
                receipt = this.dbContext.First<Receipt>(query);
            }
            return receipt;
        }
    }
}