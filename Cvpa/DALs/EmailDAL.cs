﻿using AloFramework.Core.BALs;
using AloFramework.Core.DALs;
using AloFramework.Core.Models;
using Cvpa.Models;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cvpa.DALs
{
    public class EmailDAL : AbstractDAL<Email>
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(EmailDAL));

        public Page<Email> PagedQuery(EmailListViewModel emailListViewModel)
        {
            var sql = new Sql();
            sql.Append("select * from Emails");
            if (!string.IsNullOrEmpty(emailListViewModel.Query))
            {
                sql.Where("EmailSubject like @0 OR Recipients like @1 "
                    , "%" + emailListViewModel.Query + "%"
                    , "%" + emailListViewModel.Query + "%");
            }
            //sql.OrderBy("CreatedAt DESC");
            var emails = this.dbContext.Page<Email>(emailListViewModel.PageNumber, emailListViewModel.ItemsPerPage, sql);//, s => s.OrderBy("EmailID Asc"));
            return emails;
        
        }

        public Email Save(Email email)
        {
            try
            {
               
                if (email.EmailID > 0)
                {
                    email.UpdatedAt = DateTime.Now;
                    this.dbContext.Update(email);
                }
                else
                {
                    email.Status = 1;
                    email.CreatedAt = DateTime.Now;
                    email.UpdatedAt = DateTime.Now;
                    email.SentDate = DateTime.Now;
                    this.dbContext.Insert(email);
                }
              
                return email;
            }
            catch (Exception e)
            {
                logger.Info("Error in Save Email DAL " + e.Message);
                return null;
            }
            
        }
          
        public Email GetEmail(long ID)
        {
            return this.dbContext.SingleOrDefault<Email>("where EmailID=@0 and DeletedAt is NULL", ID);
        }

        public void Delete(long? ID)
        {
          //  this.dbContext.Sanitize<Lesson>(ID);
        }

        public IEnumerable<Email> GetEmails(string filter) {
            try
            {
                logger.Info("Inside GetEmails DAL fileter = " + filter );
                return this.dbContext.Query<Email>("where EmailSubject like @0 or Recipients like @1 and DeletedAt is NULL"
                    , "%" + filter + "%"
                    , "%" + filter + "%");
            }
            catch (Exception e) {
                logger.Info("Error in Get Email DAL" + e.Message);
                return null;
                
            }
        }
    }
}