﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AloFramework.Core.DALs;
using Cvpa.Models;

namespace Cvpa.DALs
{
    public class UserDAL : AbstractDAL<User>
    {
        public User AuthenticateUser(User user) {
            return this.dbContext.First<User>(" where Username=@0 and Password=@1",user.Username, user.Password);
        }

       
    }
}