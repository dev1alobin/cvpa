﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cvpa.Models;
using AloFramework.Core.DALs;
using PetaPoco;

namespace Cvpa.DALs
{
    public class InitialNoDAL : AbstractDAL<InitialNumber>
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(InitialNumber));
        public IEnumerable<InitialNumber> All()
        {
            return this.dbContext.Query<InitialNumber>("select * from InitialNumbers");          
        }
        public InitialNumber GetInitialNo(string ResourceType)
        {
           return this.dbContext.First<InitialNumber>(" where ResourceType = @0", ResourceType);
        }
        public void UpdateInitialNo(int updatedvalue, string ResourceType) {
            logger.Info("Inside UpdateInitialNo DAL Value=" + updatedvalue + " ResourceType =" + ResourceType);
            this.dbContext.Update<InitialNumber>("SET LastNo = @0 WHERE ResourceType = @1", updatedvalue, ResourceType);
        }
    }
}