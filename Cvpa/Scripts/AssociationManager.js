var AssociationManager = {};

function getBaseURL() {
    return location.protocol + "//" + location.hostname +
       (location.port && ":" + location.port) + "/";
}

// for debugging in javascript.
var console = console || { "log": function () { }, "error": function () { } };

function esc(text) {
    return text
    .replace('&', '&amp;')
    .replace('<', '&lt;')
    .replace('>', '&gt;');
}

function showPageStatus(msg) {
    jQuery("#page-status").html(msg).fadeOut(3600, function () {
        jQuery("#page-status").html('').show();
    });
}

function onlyNumerics(e) {
    if (e.shiftKey || e.ctrlKey || e.altKey) {
        e.preventDefault();
    } else {
        var key = e.keyCode;
        if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
            e.preventDefault();
        }
    }
}
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function getCurrentDateTime() {
    var date = new Date();
    return (date.getDate() < 10 ? ('0' + date.getDate()) : date.getDate()) + "/"
        + ((date.getMonth() + 1) < 10 ? ('0' + (date.getMonth() + 1)) : (date.getMonth() + 1)) + "/"
        + date.getFullYear() + " "
        + (date.getHours() < 10 ? ('0' + date.getHours()) : date.getHours()) + ":"
        + (date.getMinutes() < 10 ? ('0' + date.getMinutes()) : date.getMinutes()) + ":"
        + (date.getSeconds() < 10 ? ('0' + date.getSeconds()) : date.getSeconds());
    //return moment().format("DD/MM/YYYY HH:mm");
}

function getMinutesDifference(StartDate, EndDate) {
    var datediff = (Math.abs(new Date(StartDate)) - Math.abs(new Date(EndDate))); // returns milliseconds
    return Math.ceil((datediff / 1000) / 60); // milliseconds/1000/60 = minutes
}

if (typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, '');
    }
}

jQuery(document).ready(function ($) {

    $.ajaxSetup({
        type: 'GET',
        dataType: 'json',
        ifModified: true,
        //contentType: 'application/json; charset=utf-8',
        error: function (err) {
            console.log(err);
        }
    });

    AssociationManager.Models = function () {
        this.url = '/';

        this.Save = function (actionURL, data, callback) {
            $.ajax({
                url: actionURL,
                data: JSON.stringify(data),
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    console.log(data);
                    callback(data);
                }
            });
        }

        this.getURL = function () {
            return getBaseURL() + this.url;
        }
    }

    AssociationManager.Collections = function () {
        this.url = '/';
        this.All = function (callback) {
            $.ajax({
                url: this.getURL(),
                success: function (data) {
                    callback(data);
                }
            });
        },
        this.convertToSelect = function (el, callback, success) {
            this.All(function (data) {
                var html = [], h = -1;
                $.each(data, function (i) {
                    var item = data[i];
                    html[++h] = callback(item);
                });
                el.append(html.join(''));

                if (success != undefined)
                    success(data);
            });
        },
        this.autocomplete2 = function (el, actionURL, options, render, dataRender) {
            options.ajax = {
                url: this.getURL() + actionURL,
                dataType: 'json',
                quietMillis: 250,
                data: function (term, page) { // page is the one-based page number tracked by Select2
                    if (dataRender != undefined)
                        return dataRender(term, page);
                    else {
                        return {
                            filter: term, //search term
                            page: page // page number,
                        };
                    }
                },
                results: function (data) {
                    var myResults = [];
                    $.each(data, function (index, item) {
                        myResults.push(render(index, item));
                    });
                    return { results: myResults };
                },
                cache: true
            };

            return el.select2(options);
        }

        this.getURL = function () {
            return getBaseURL() + this.url;
        }
    }
   /* // Boarding House related models and collections
    var BoardingHouses = new BLR.Collections;
    BoardingHouses.url = "/BoardingHouses";
    BoardingHouses.convertHouseToSelect = function (el, success) {
        this.convertToSelect(el, function (House) {
            return '<option value=' + House.BoardingHouseCode + '>' + House.BoardingHouseName + '</option>';
        }, success);
    }
    BLR.Collections.BoardingHouses = BoardingHouses;

    // LeaveTypes related models and collections
    var LeaveTypes = new BLR.Collections;
    LeaveTypes.url = "/LeaveTypes";
    LeaveTypes.convertTypeToSelect = function (el, success) {
        this.convertToSelect(el, function (LeaveType) {
            return '<option value=' + LeaveType.LeaveTypeID + '>' + LeaveType.LeaveTypeName + '</option>';
        }, success);
    }
    BLR.Collections.LeaveTypes = LeaveTypes;

    //Students for logged parents collection
    /*var Students = new BLR.Collections;
    Students.url = "/Users/GetStudentsForParent";
    Students.convertToSelect = function (el, success) {
        this.convertToSelect(el, function (Student) {
            return '<option value=' + Student.UserID + '>' + Student.FirstName + ' ' + Student.SurName + '</option>';
        }, success);
    }*/


   /* // Leave related models and collections
    var Leave = new BLR.Models;
    Leave.url = '/Leaves';

    Leave.ApproveLeave = function (data, success) {
        console.log(data);
        $.ajax({
            url: this.getURL() + '/ApproveLeave',
            data: data,
            type: 'POST',
            success: function (result) {
                success(result);
            }
        });
    }
    BLR.Models.Leave = Leave;

    

    var Setting = new BLR.Models;
    Setting.url = '/Settings';
    Setting.GetSetting = function (settingName, success) {        
        $.ajax({
            url: this.getURL() + "/GetSettings",
            data: { SettingName: settingName },
            type: 'GET',
            success: function (data) {
                success(data);
            }
        });
    }
    Setting.GetOptionValue = function (setting, optionName) {
        if (setting.Options != undefined)
        {
            for (var i = 0; i < setting.Options.length; i++) {
                if (setting.Options[i].Name == optionName) {
                    return setting.Options[i].Value;
                }
            }
        }
    }
    BLR.Models.Setting = Setting;

    // Users related models and collections
    var Users = new BLR.Collections;
    Users.url = '/Users';
    /*Users.convertUsersToSelect = function (el, success) {
        this.convertToSelect(el, function (User) {
            return '<option value=' + User.UserID + '>' + User.FirstName + '</option>';
        }, success);
    }*/
  /*  BLR.Collections.Users = Users;
    */
});