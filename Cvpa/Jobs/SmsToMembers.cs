﻿using Quartz;
using System;
using System.Net;
using System.Net.Mail;
using Cvpa.BALs;
using Cvpa.Models;
using System.Collections.Generic;
using System.IO;
using Cvpa.Utils;

namespace ScheduledTaskExample.ScheduledTasks
{
    public class EmailJob : IJob
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(EmailJob));
        private MemberBAL memberBAL = new MemberBAL();
        private static String Send_Sms_To_Members = ControllerUtils.GetScreenData("Sms", "Send_Sms");

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                if (Send_Sms_To_Members == "Yes")
                {
                    List<Member> memberdetails = new List<Member>();
                    memberdetails = this.memberBAL.GetMembersExpiry();

                    foreach (var expiry in memberdetails)
                    {

                        // Sms Alert for Membership Expiry 

                        string expirymessage = "பா.குப்புசாமி புதுபேட்டை சங்கம், உங்கள் சந்தா " + expiry.MembershipExpiryDate + " அன்று முடிவடைகிறது, புதுபித்து கொள்ளவும்";
                        string mobileno = expiry.MobileNo;
                        if (expiry.MembershipExpiryDate == DateTime.Now.Date.AddDays(+7))
                        {
                            logger.Info("Membership Expiry Alert SMS Before 1Week. Date Before 1week = " + DateTime.Now.Date.AddDays(+7) + " - ExpiryDate = " + expiry.MembershipExpiryDate);
                            logger.Info("உங்கள் சந்தா " + expiry.MembershipExpiryDate + " அன்று முடிவடைகிறது, புதுபித்து கொள்ளவும்");
                            // string messageToCallInPatient = "The doctor is ready to see you in 5 minutes. Please wait outside room ";

                            if ((mobileno != null ) && (mobileno != ""))
                            {
                                logger.Info("Inside mobileno " + mobileno.Length);
                                string url = "http://dnd.alobin.com/api/sentsms.php?username=alobin&api_password=a0f5a75416&to=" + mobileno + "&priority=2&sender=MCPPNS&message=" + expirymessage;

                                HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
                                webReq.Method = "GET";
                                HttpWebResponse webResponse = (HttpWebResponse)webReq.GetResponse();

                                //I don't use the response for anything right now. But I might log the response answer later on.   
                                Stream answer = webResponse.GetResponseStream();
                                StreamReader _recivedAnswer = new StreamReader(answer);
                            }
                        }
                        else if (expiry.MembershipExpiryDate == DateTime.Now.Date.AddDays(+1))
                        {
                            logger.Info("Membership Expiry Alert SMS Before 1Day. Date Before 1Day = " + DateTime.Now.Date.AddDays(+1) + " - ExpiryDate = " + expiry.MembershipExpiryDate);
                            if ((mobileno != null || mobileno != ""))
                             {
                                logger.Info("Inside mobileno Length " + mobileno.Length + " MobileNo = " + mobileno);
                                string url = "http://dnd.alobin.com/api/sentsms.php?username=alobin&api_password=a0f5a75416&to=" + mobileno + "&priority=2&sender=MCPPNS&message=" + expirymessage;

                                HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
                                webReq.Method = "GET";
                                HttpWebResponse webResponse = (HttpWebResponse)webReq.GetResponse();

                                //I don't use the response for anything right now. But I might log the response answer later on.   
                                Stream answer = webResponse.GetResponseStream();
                                StreamReader _recivedAnswer = new StreamReader(answer);
                            }
                        }

                        // Birthday Sms for Members
                        string name = expiry.MemberName;
                        string birthdaymessage = "பா.குப்புசாமி புதுபேட்டை சங்கம், திரு. " + name + " அவர்களுக்கு இனிய பிறந்தநாள் நல்வாழ்த்துக்கள்";
                        string birthdate = expiry.DateOfBirth.ToString("dd-MMMM");
                        string currentdate = DateTime.Now.ToString("dd-MMMM");

                        if (birthdate == currentdate)
                        {

                            logger.Info("Members Birthday SMS  Date " + expiry.DateOfBirth);
                            logger.Info("திரு. " + name + " அவர்களுக்கு இனிய பிறந்தநாள் நல்வாழ்த்துக்கள்");
                            if ((mobileno != null) && (mobileno != ""))
                            {
                                logger.Info("Inside mobileno Length " + mobileno.Length + " MobileNo = " + mobileno);
                                string url = "http://dnd.alobin.com/api/sentsms.php?username=alobin&api_password=a0f5a75416&to=" + mobileno + "&priority=2&sender=MCPPNS&message=" + birthdaymessage;

                                HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
                                webReq.Method = "GET";
                                HttpWebResponse webResponse = (HttpWebResponse)webReq.GetResponse();

                                //I don't use the response for anything right now. But I might log the response answer later on.   
                                Stream answer = webResponse.GetResponseStream();
                                StreamReader _recivedAnswer = new StreamReader(answer);
                            }
                        }


                    }
                }
               
            } catch(Exception e){
                logger.Info("Error in Email"+e.Message);
            }
        }


    }
}