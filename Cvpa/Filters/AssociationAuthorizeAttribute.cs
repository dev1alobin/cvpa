﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;

namespace Cvpa.Filters
{
    public class AssociationAuthorizeAttribute : AuthorizeAttribute
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(AssociationAuthorizeAttribute));

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (System.Web.HttpContext.Current.Session["LoggedUser"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { action = "Login", controller = "User", area = "" }));
                logger.Info("The page is redirected to Login page due to session expires.");
            }
        }
    }
}