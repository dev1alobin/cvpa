﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cvpa.Filters
{
    public class AloErrorHandleAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            Exception exception = filterContext.Exception;
            //Logging the Exception
            filterContext.ExceptionHandled = true;


            var model = new HandleErrorInfo(filterContext.Exception, 
                filterContext.RouteData.Values["controller"].ToString(), 
                filterContext.RouteData.Values["action"].ToString());

            filterContext.Result = new ViewResult()
            {   
                ViewName = "Index",
                ViewData = new ViewDataDictionary(model)
            };
        }
    }
}