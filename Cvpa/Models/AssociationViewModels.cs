﻿using Cvpa.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cvpa.Models
{
    public class ReceiptViewModel
    {
        public Receipt Receipt { get; set; }
        //public 
    }

    /*public abstract class ListTableViewModel<T> : PetaPoco.Page<T>
    {
        public ListTableViewModel()
        {
            this.PageNumber = PageUtils.PAGE_NUMBER;
            this.ItemsPerPage = PageUtils.ITEMS_PER_PAGE;
        }

        public string Query { get; set; }
        public List<string> FilterColumnValues { get; set; }
        public List<string> FitlerOperators { get; set; }
        public List<string> FilterColumnValues2 { get; set; }

        public long PageNumber { get; set; }

    
    }*/

    public class MemberListViewModel
    {
        public MemberListViewModel()
        {
            this.PageNumber = PageUtils.PAGE_NUMBER;
            this.ItemsPerPage = PageUtils.ITEMS_PER_PAGE;
            this.SearchColumnCondition = FilterUtils.OR_CONDITION;
            this.SearchColumnNames = "MemberNo,MemberName,MobileNo,Speciality,StoreArea,Status,StotrPincode,Community,BloodGroup ";

        }

        public List<string> Zones { get; set; }
        public List<string> StoreArea { get; set; }
       
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string SearchColumnNames { get; set; }
        public string SearchColumnCondition { get; set; }
        public string SearchFilterOperator { get; set; }
        public string[] FilterColumnValues { get; set; }
        public string[] FilterOperators { get; set; }
        public string[] FilterColumnValues2 { get; set; }
        public string[] SortColumnNames { get; set; }
        public string[] SortColumnDirs { get; set; }
      //  public string MembershipExpiryDate { get; set; }
        public long PageNumber { get; set; }
        public long ItemsPerPage { get; set; }
        public PetaPoco.Page<Member> Members { get; set; }

        public MemberFilerModel MemberFilter { get; set; }

        public string ColumnNames
        {
            get
            {
                return "MemberNo,MemberName,MobileNo,ExpiryDate,Speciality,StoreArea,Status,StotrPincode,Community,BloodGroup";
            }
        }
    }


    public class MemberFilerModel
    {
        public string MemberType { get; set; }
        public string Query { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string Zone { get; set;}
        public string StoreArea { get; set; }
        public DateTime? FromDateTime { get; set; }
        public DateTime? ToDateTime { get; set; }
        public string PrintOption { get; set; }
    }

    public class ReceiptListViewModel { 
         public ReceiptListViewModel()
        {
            this.PageNumber = PageUtils.PAGE_NUMBER;
            this.ItemsPerPage = PageUtils.ITEMS_PER_PAGE;
            this.SearchColumnCondition = FilterUtils.OR_CONDITION;
            this.SearchColumnNames = "ReceiptNo,MemberName,MemberNo,ReceiptDate";
        }

        public string Query { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string SearchColumnNames { get; set; }
        public string SearchColumnCondition { get; set; }
        public string SearchFilterOperator { get; set; }
        public string[] FilterColumnValues { get; set; }
        public string[] FilterOperators { get; set; }
        public string[] FilterColumnValues2 { get; set; }
        public string[] SortColumnNames { get; set; }
        public string[] SortColumnDirs { get; set; }

        public long PageNumber { get; set; }
        public long ItemsPerPage { get; set; }
        public PetaPoco.Page<Receipt> Receipts { get; set; }

        public FilterModel ReceiptFilter { get; set; }

    }

    

    public class AccountListViewModel
    {
        public AccountListViewModel()
        {
            this.PageNumber = PageUtils.PAGE_NUMBER;
            this.ItemsPerPage = PageUtils.ITEMS_PER_PAGE;
            this.SearchColumnCondition = FilterUtils.OR_CONDITION;
            this.SearchColumnNames = "ReceiptNo,Reason,ReceiptDate,Name,PaymentType,Amount";
        }
    //    public string SortType { get; set; }
        public string Query { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string PaymentType { get; set; }
        public string SearchColumnNames { get; set; }
        public string SearchColumnCondition { get; set; }
        public string SearchFilterOperator { get; set; }
        public string[] FilterColumnValues { get; set; }
        public string[] FilterOperators { get; set; }
        public string[] FilterColumnValues2 { get; set; }
        public string[] SortColumnNames { get; set; }
        public string[] SortColumnDirs { get; set; }

        public long PageNumber { get; set; }
        public long ItemsPerPage { get; set; }
        public PetaPoco.Page<Account> Accounts { get; set; }
        public AccountFilterModel AccountFilter { get; set; }
    }

    public class AccountFilterModel
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public DateTime? FromDateTime { get; set; }
        public DateTime? ToDateTime { get; set; }
        public string PrintOption { get; set; }
        public string AccountType { get; set; }
}

    public class InsuranceListViewModel
    {
        public InsuranceListViewModel()
        {
            this.PageNumber = PageUtils.PAGE_NUMBER;
            this.ItemsPerPage = PageUtils.ITEMS_PER_PAGE;
            this.SearchColumnCondition = FilterUtils.OR_CONDITION;
            this.SearchColumnNames = "ReceiptNo,MemberName,MemberNo,ReceiptDate";
        }

        public string Query { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string SearchColumnNames { get; set; }
        public string SearchColumnCondition { get; set; }
        public string SearchFilterOperator { get; set; }
        public string[] FilterColumnValues { get; set; }
        public string[] FilterOperators { get; set; }
        public string[] FilterColumnValues2 { get; set; }
        public string[] SortColumnNames { get; set; }
        public string[] SortColumnDirs { get; set; }

        public long PageNumber { get; set; }
        public long ItemsPerPage { get; set; }
        public PetaPoco.Page<Insurance> Insurances { get; set; }
        public FilterModel InsuranceFilter { get; set; }

    }

    public class FilterModel
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public DateTime? FromDateTime { get; set; }
        public DateTime? ToDateTime { get; set; }
        public string PrintOption { get; set; }
    }

    public class MagazineListViewModel
    {
        public MagazineListViewModel()
        {
            this.PageNumber = PageUtils.PAGE_NUMBER;
            this.ItemsPerPage = PageUtils.ITEMS_PER_PAGE;
            this.SearchColumnCondition = FilterUtils.OR_CONDITION;
            this.SearchColumnNames = "MemberID,MemberName,MemberNo";
        }

        public string Query { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string SearchColumnNames { get; set; }
        public string SearchColumnCondition { get; set; }
        public string SearchFilterOperator { get; set; }
        public string[] FilterColumnValues { get; set; }
        public string[] FilterOperators { get; set; }
        public string[] FilterColumnValues2 { get; set; }
        public string[] SortColumnNames { get; set; }
        public string[] SortColumnDirs { get; set; }

        public long PageNumber { get; set; }
        public long ItemsPerPage { get; set; }
        public PetaPoco.Page<Magazine> Magazines { get; set; }

    }

    public class CategoryListViewModel
    {
        public CategoryListViewModel()
        {
            this.PageNumber = PageUtils.PAGE_NUMBER;
            this.ItemsPerPage = PageUtils.ITEMS_PER_PAGE;
            this.SearchColumnCondition = FilterUtils.OR_CONDITION;
            this.SearchColumnNames = "CategoryID,CategoryName";
        }

        public string Query { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string SearchColumnNames { get; set; }
        public string SearchColumnCondition { get; set; }
        public string SearchFilterOperator { get; set; }
        public string[] FilterColumnValues { get; set; }
        public string[] FilterOperators { get; set; }
        public string[] FilterColumnValues2 { get; set; }
        public string[] SortColumnNames { get; set; }
        public string[] SortColumnDirs { get; set; }

        public long PageNumber { get; set; }
        public long ItemsPerPage { get; set; }
        public PetaPoco.Page<Category> Categories { get; set; }

    }

    public class MemberEditViewModel
    {
        public Member MemberData { get; set; }
    }
    public class ReceiptEditViewModel
    {
        public Receipt ReceiptData { get; set; }
        public List<Category> Categorys { get; set; }
    }

    public class ExpenseEditViewModel
    {
     //   public Expense ExpenseData { get; set; }
    }

    public class MagazineEditViewModel
    {
        public Magazine MagazineData { get; set; }
    }

    public class CategoryEditViewModel
    {
        public Category CategoryData { get; set; }
    }

    public class AccountEditViewModel
    {
        public Account AccountData { get; set; }
        public List<Category> Categorys { get; set; }
    }

    public class InsuranceEditViewModel
    {
        public Insurance InsuranceData { get; set; }
    }

    public class DashboardCountsModel
    {
        public int MembersCount { get; set; }
        public int ActiveMembersCount { get; set; }
        public int LifeMembersCount { get; set; }
        public int ExpiredMembersCount { get; set; }
    }

    public class EmailViewModel 
    {
        public Email EmailData { get; set; }
    }

    public class EmailListViewModel 
    {
        public PetaPoco.Page<Email> Emails { get; set; }

        public EmailListViewModel()
        {
            this.PageNumber = PageUtils.PAGE_NUMBER;
            this.ItemsPerPage = PageUtils.ITEMS_PER_PAGE;
            this.SearchColumnCondition = FilterUtils.OR_CONDITION;
            this.SearchColumnNames = "EmailID";
        }

        public string Query { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string SearchColumnNames { get; set; }
        public string SearchColumnCondition { get; set; }
        public string SearchFilterOperator { get; set; }
        public string[] FilterColumnValues { get; set; }
        public string[] FilterOperators { get; set; }
        public string[] FilterColumnValues2 { get; set; }
        public string[] SortColumnNames { get; set; }
        public string[] SortColumnDirs { get; set; }

        public long PageNumber { get; set; }
        public long ItemsPerPage { get; set; }
        public PetaPoco.Page<Category> Categories { get; set; }

    }


}