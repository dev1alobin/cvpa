﻿using Cvpa.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cvpa.Models
{
    public class MemberEntryViewModel
    {
        public MemberEntryViewModel()
        {
            this.PageNumber = PageUtils.PAGE_NUMBER;
            this.ItemsPerPage = PageUtils.ITEMS_PER_PAGE;
            this.SearchColumnCondition = FilterUtils.OR_CONDITION;
            this.SearchColumnNames = "MemberName,MeetingID,PhoneNo";
        }

        public string Query { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string PaymentType { get; set; }
        public string SearchColumnNames { get; set; }
        public string SearchColumnCondition { get; set; }
        public string SearchFilterOperator { get; set; }
        public string[] FilterColumnValues { get; set; }
        public string[] FilterOperators { get; set; }
        public string[] FilterColumnValues2 { get; set; }
        public string[] SortColumnNames { get; set; }
        public string[] SortColumnDirs { get; set; }

        public long PageNumber { get; set; }
        public long ItemsPerPage { get; set; }
        public long MeetingID { get; set; }
        public string MeetingTitle { get; set; }
        public string MeetingPlace { get; set; }
        public string MeetingDate { get; set; }
        public string Message { get; set; }
        public PetaPoco.Page<MemberEntry> MemberEntries { get; set; }
        public string ColumnNames
        {
            get
            {
                return "MemberNo,MemberName,PhoneNo,ShopName";
            }
        }
    }
        public class MemberEntryEditViewModel
        {
            public MemberEntry MemberEntryData { get; set; }

        }

}