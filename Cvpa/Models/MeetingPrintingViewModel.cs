﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cvpa.Models
{
    public class MeetingPrintingViewModel
    {
        public List<MemberEntry> MemberEntries { get; set; }
    }
}