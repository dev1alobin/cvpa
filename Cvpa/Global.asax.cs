﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ScheduledTaskExample.ScheduledTasks;
using System.Globalization;
using System.Threading;
namespace Cvpa
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(MvcApplication));

        protected void Application_Start()
        {
            LogConfig.RegisterLog();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFitlers(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        //    JobScheduler.Start();
        }
        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            CultureInfo appCulture = new CultureInfo("en-AU", false);//(CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            appCulture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            appCulture.DateTimeFormat.LongDatePattern = "dd/MM/yyyy HH:mm";
            appCulture.DateTimeFormat.DateSeparator = "/";
            Thread.CurrentThread.CurrentCulture = appCulture;
            Thread.CurrentThread.CurrentUICulture = appCulture;
        }
        /*
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            logger.Error(exception);
            Server.ClearError();
            Response.RedirectToRoute("Error", new { exception });
        }*/
    }
}
