﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cvpa.BALs;
using Cvpa.Models;
using System.Text;


namespace Cvpa.Controllers
{
    public class UserController : Controller
    {
        private UserBAL userBAL = new UserBAL();
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(UserController));
        // GET: /Account/
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(User user)
        {
            logger.Info("Inside AuthenticateUser UserID = " +user.UserID + "Username =" +user.Username );
           // User LoggedUser = new User();
            var LoggedUser = this.userBAL.AuthenticateUser(user);
            if (LoggedUser != null)
            {
                Session["LoggedUser"] = LoggedUser;
                return RedirectToAction("Index", "Dashboard");
            }
            else
            {
                logger.Info("Error In AuthenticateUser");
                ViewBag.Message = "* Invalid Username or Password";
                return View();
            }
            
        }

        public ActionResult Logout() {
           // Session.RemoveAll();
            Response.Cookies.Clear();
            return RedirectToAction("Login", "User");
        }


       
	}
}