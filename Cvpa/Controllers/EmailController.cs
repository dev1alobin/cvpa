﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cvpa.BALs;
using Cvpa.Models;
using System.Text;
using Cvpa.Utils;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading;

namespace Cvpa.Controllers
{
    public class EmailController : Controller
    {
        private EmailBAL _emailBAL = new EmailBAL();
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(EmailController));
        //  private static String EMAIL_NOTIFICATION = ScreenCustomization.GetScreenData("Email", "Enable_Notification");
        private static string EMAIL_NOTIFICATION = System.Configuration.ConfigurationManager.AppSettings["EMAIL_NOTIFICATION"];
        private static string TEST_MAIL = System.Configuration.ConfigurationManager.AppSettings["TEST_MAIL"];
        private static string TEST_MAIL_ADDRESS = System.Configuration.ConfigurationManager.AppSettings["TEST_MAIL_ADDRESS"];
        private static string FROM_ADDRESS = System.Configuration.ConfigurationManager.AppSettings["FROM_ADDRESS"];
        private static string SMTP_HOST = System.Configuration.ConfigurationManager.AppSettings["SMTP_HOST"];
        private static string SMTP_USERNAME = System.Configuration.ConfigurationManager.AppSettings["SMTP_USERNAME"];
        private static string SMTP_PASSWORD = System.Configuration.ConfigurationManager.AppSettings["SMTP_PASSWORD"];
        // private static string SMTP_PORT = System.Configuration.ConfigurationManager.AppSettings["SMTP_PORT"];
        private bool enableSSL = true;

        // GET: Lesson

        public ActionResult Index(EmailListViewModel emailListViewModel)
        {
           // emailListViewModel = (EmailListViewModel)this.GetListViewModel(emailListViewModel);
            emailListViewModel.Emails = _emailBAL.PagedQuery(emailListViewModel);
            return View(emailListViewModel);
        }

        public ActionResult Create()
        {
            string EmailID = (Request.Params["EmailID"] != null) ? Request.Params["EmailID"] : null;
            ViewBag.InitialEmailID = EmailID;
            return View();
        }

        [HttpPost]
        public ActionResult Create(Email email)
        {
            try
            {
                logger.Info("Inside Save Email ");
                email.Recipients = string.Join(",", email.Recipients.Split(','));
                var packageData = _emailBAL.Save(email);
                this.SendMail(email.Recipients, email.EmailSubject, email.EmailMessage);
                return RedirectToAction("Index", "Email");
            }
            catch (Exception e)
            {
                logger.Info("Error in Save Email " + e.Message);
            }
            return null;
        }

        public void SendMail(string ToAddress
          , string Subject
          , string Body
          , IEnumerable<HttpPostedFileBase> attachmentFiles = null
          , bool isAsync = false)
        {
            var smtpSendThread = new Thread(() => SendMailThread(ToAddress, Subject, Body, attachmentFiles, isAsync));
            smtpSendThread.Start();
        }

        public void SendMailThread(string ToAddress
                  , string Subject
                  , string Body
                  , IEnumerable<HttpPostedFileBase> attachmentFiles = null
                  , bool isAsync = false)
        {
            try
            {
                logger.Info("Email To " + ToAddress + "Email From = " + FROM_ADDRESS);
                if (EMAIL_NOTIFICATION.Equals("Yes"))
                {
                    if (String.IsNullOrEmpty(ToAddress))
                    {
                        ToAddress = FROM_ADDRESS;
                    }
                    logger.Info("***  Email Notification Enabled  ***");
                    if (TEST_MAIL.Equals("Yes"))
                    {
                        logger.Info("*** Test Email Enabled  ***");
                        ToAddress = TEST_MAIL_ADDRESS;
                    }
                    using (MailMessage mail = new MailMessage(FROM_ADDRESS, ToAddress))// +"," + CC + "," + BCC))
                    {

                        mail.Subject = Subject;
                        mail.Body = Body;
                        mail.IsBodyHtml = true;

                        if (attachmentFiles != null)
                        {
                            foreach (var attachmentFile in attachmentFiles)
                            {
                                if (attachmentFile != null)
                                {
                                    System.Net.Mail.Attachment attachment;
                                    attachment = new System.Net.Mail.Attachment(attachmentFile.InputStream, attachmentFile.FileName);
                                    mail.Attachments.Add(attachment);
                                }
                            }
                        }

                        SmtpClient smtp = new SmtpClient();
                        smtp.Host = SMTP_HOST;
                        smtp.EnableSsl = enableSSL;
                        NetworkCredential networkCredential = new NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = networkCredential;
                        //smtp.Port = Int32.Parse(SMTP_PORT);
                        smtp.Port = 587;

                        //if (isAsync)
                        //{
                        //    Object state = mail;
                        //    smtp.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
                        //    try
                        //    {
                        //        smtp.SendAsync(mail, mail);
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        logger.Error("Mail sending error, due to " + ex);
                        //    }
                        //}
                        //else
                        //{

                        smtp.Send(mail);
                        logger.Info("Mail sent successfully to " + ToAddress);
                        //}

                    }
                }
                else
                {
                    logger.Info("Email Notification NOT Enabled ");
                }
            }
            catch (Exception e)
            {
                logger.Error("Problem in sending Email, due to " + e);
            }
        }

        /* public void SendMail(string Recipients,string Subject, string Message) {

             using (MailMessage mail = new MailMessage("naveen@alobin.com", "naveen@alobin.com"))// +"," + CC + "," + BCC))
             {
                 mail.Subject = "Test";
                 mail.Body = "Test";
                 mail.IsBodyHtml = true;
                 SmtpClient smtp = new SmtpClient();
                 smtp.Host = "smtp.gmail.com";
                 smtp.EnableSsl = true;
                 NetworkCredential networkCredential = new NetworkCredential("naveen@alobin.com", "Technologies@Alobin987");
                 smtp.UseDefaultCredentials = true;
                 smtp.Credentials = networkCredential;
                 smtp.Port = Int32.Parse("587");

                 smtp.Send(mail);

             }
         }*/

        public ActionResult View(long? ID)
        {
            if (ID != null)
            {
                var emailViewModel = new EmailViewModel();
                emailViewModel.EmailData = _emailBAL.GetEmail(ID);
                return View("View", emailViewModel);
            }
            return RedirectToAction("Index");
        }





        // GET: /Instructor/Edit/1
        public ActionResult Edit(long? ID)
        {
            if (ID != null)
            {
                ViewBag.ResourceData = _emailBAL.GetEmail(ID);
                return View("Create");//, this.GetEditViewModel(ID));
            }
            return RedirectToAction("Create", "Email");
        }

        public ActionResult Delete(long? ID)
        {
            try
            {
                _emailBAL.Delete(ID);
                Auth.SetStatusMessage(String.Format(Lang.GetLang("email_deleted")));
            }
            catch (Exception ex)
            {
                Auth.SetStatusMessage(String.Format(Lang.GetLang("error_email_delete")), "Warning");
            }
            return RedirectToAction("Index");
        }

        // GET : /GetLessonList 
        public JsonResult GetEmails(string filter)
        {
            logger.Info("Inside GetEmails List");
            return Json(_emailBAL.GetEmails(filter), JsonRequestBehavior.AllowGet);
        }

    }
}