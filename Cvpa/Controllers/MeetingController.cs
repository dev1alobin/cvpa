﻿using Cvpa.BALs;
using Cvpa.Models;
using Cvpa.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Cvpa.Controllers
{
    public class MeetingController : Controller
    {
        private MeetingBAL meetingBAL = new MeetingBAL();
        private MemberBAL memberBAL = new MemberBAL();
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(MeetingController));
        private static String Send_Sms_To_Members = ControllerUtils.GetScreenData("Sms", "Send_Sms");
        // GET: Meeting
        public ActionResult Index(MeetingListViewModel meetingListViewModel)
        {
            meetingListViewModel.Meetings = this.meetingBAL.PagedQuery(meetingListViewModel);
            return View(meetingListViewModel);
        }
        
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Meeting meeting)
        {
          
            Meeting meetingid = this.meetingBAL.FindByID(meeting.MeetingID);
            meeting.CreatedDate = DateTime.Now;

            try
            {
                if (meeting.MeetingID > 0)
                {
                    meeting.UpdatedDate = DateTime.Now;
                    meeting.Status = 1;
                    this.meetingBAL.Update(meeting);
                    Auth.SetStatusMessage("Meeting Updated Successfully");

                }
                else
                {
                    meeting.Status = 1;
                    this.meetingBAL.Save(meeting);
                    Auth.SetStatusMessage("Meeting Saved Successfully");
                }
            }
            catch(Exception e)
            {
                logger.Info("Error Message in Meetin Controller" + e);
                Auth.SetStatusMessage("Problem in Saving Meeting, please try again.", "Warning");
            }
            
            return RedirectToAction("Index","Meeting");
        }
        public JsonResult MeetingSendSMS(Meeting meeting, string SmsTo, string Area, string MobileNumber, string Message)
        {
            meeting.CreatedDate = DateTime.Now;
            if (meeting.MeetingID > 0)
            {
                meeting.UpdatedDate = DateTime.Now;
                meeting.Status = 1;
                this.meetingBAL.Update(meeting);
                Auth.SetStatusMessage("Meeting Updated Successfully");

            }
            else
            {
                meeting.Status = 1;
                this.meetingBAL.Save(meeting);
                Auth.SetStatusMessage("Meeting Saved Successfully");
            }

            //this.meetingBAL.Save(meeting);
            //   this.memberBAL.SendSms(meeting.SendSmsTo, meeting.MeetingPlace);
            string result = "";
            try
            { 
            logger.Info("Screencustomization value for Sending SMS = " + Send_Sms_To_Members);
            if (false)// Send_Sms_To_Members.Equals("Yes"))
            {

                    /*logger.Info("Send Festival Sms to all members");
                    List<Member> memberdetails = new List<Member>();
                    memberdetails = this.memberBAL.SendSms(SmsTo, Area);

                    if (MobileNumber != "")
                    {
                        logger.Info("Individual Member SMS Mobile Number = " + MobileNumber);
                            string url = "http://dnd.alobin.com/api/sentsms.php?username=alobin&api_password=a0f5a75416&to=" + MobileNumber + "&priority=2&sender=MCPPNS&message=" + Message;

                            HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
                            webReq.Method = "GET";
                            HttpWebResponse webResponse = (HttpWebResponse)webReq.GetResponse();

                            //I don't use the response for anything right now. But I might log the response answer later on.   
                            Stream answer = webResponse.GetResponseStream();
                            StreamReader _recivedAnswer = new StreamReader(answer);

                            result = "Sms Sent Successfully";
                    }

                    logger.Info("Total Members count " + memberdetails.Count());
                    foreach (var members in memberdetails)
                    {
                        string mobileno = members.MobileNo;
                        if ((!mobileno.Equals(null)) && (!mobileno.Equals("")))
                        {

                                logger.Info("Send Festival SMS to all Members MobileNo = " + mobileno + " Religion = " + SmsTo + " Area = " + Area);

                                string url = "http://dnd.alobin.com/api/sentsms.php?username=alobin&api_password=a0f5a75416&to=" + mobileno + "&priority=2&sender=MCPPNS&message=" + Message;

                                HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
                                webReq.Method = "GET";
                                HttpWebResponse webResponse = (HttpWebResponse)webReq.GetResponse();

                                //I don't use the response for anything right now. But I might log the response answer later on.   
                                Stream answer = webResponse.GetResponseStream();
                                StreamReader _recivedAnswer = new StreamReader(answer);

                                result = "Sms Sent Successfully";
                        }
                    }*/

                }
                else {
                result = "Sms Notification Disabled. Contact Administrator. Meeting saved successfully";
                logger.Info("Sms Notification Disabled. Contact Administrator");

            }
        }
            catch (Exception e)
            {
                logger.Info("Sending SMS to Members Failed Due to " + e.Message);
            }
           // return View(result);
            return Json(result, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
            //return View("Index", "Meeting");
        }

        public ActionResult MeetingDetails(MemberEntryViewModel memberEntryViewModel)
        {
            memberEntryViewModel.MemberEntries = this.meetingBAL.PagedQuery(memberEntryViewModel);
            //ViewBag.Message = memberEntryViewModel.Message;
            return View(memberEntryViewModel);
        }

        [HttpPost]
        public ActionResult SaveMemberEntry(MemberEntry memberEntry)
        {
            var Message = "";
            try
            {
                memberEntry.CreatedDate = DateTime.Now;
                memberEntry.UpdatedDate = DateTime.Now;
                memberEntry.Status = 1;
                var SavedMember = this.meetingBAL.SaveMemberEntry(memberEntry);
                if (!SavedMember)
                {
                    Message = "Member Already Exists";
                }
            }
            catch(Exception e)
            {
                logger.Info("Error message in meeting controller Memberentry" + e.Message);
            }
            return RedirectToAction("MeetingDetails", "Meeting", new { MeetingID = memberEntry.MeetingID , Message = Message });
        }

        public ActionResult Edit(long? MeetingID)
        {
           if (MeetingID != null)
            {
                long ID = (long)MeetingID;
                Meeting meeting = this.meetingBAL.FindByID(ID);
                MeetingEditViewModel memberEntryEditViewModel = new MeetingEditViewModel
                {
                    MeetingData = meeting
                };
                return View("Edit", memberEntryEditViewModel);
            }
            return RedirectToAction("Create", "Meeting");
        }

        public JsonResult Delete(long ID)
        {
            logger.Info("Inside Delete Member");
            logger.Info("Delete Member " + ID);
            return Json(this.meetingBAL.Delete(ID), "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMemberNoForMeeting(string ColumnName, long term)
        {
            return Json(memberBAL.GetMemberNoForMeeting(ColumnName, term), "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MembersSinglePrint(long MeetingID)
        {
            try
            {
                 Meeting meetingID = new Meeting();

                if (MeetingID  > 0)
                {
                    logger.Info("Inside MembersSingleprint MemberID = " + MeetingID);
                    meetingID = this.meetingBAL.FindByID(MeetingID);

                }
                // logger.Info("MembersSingleprint Result" + meetingID.ToString());
                MeetingEditViewModel meetingListViewModel = new MeetingEditViewModel
                {
                    MeetingData = meetingID
                }; 

                return View("MembersSinglePrint", meetingListViewModel);
            }
            catch (Exception e)
            {
                logger.Info("Error in ReceiptSinglePrint" + e.Message);
            }
            return View("MembersSinglePrint", new MeetingEditViewModel());
        }
        public ActionResult MembersListPrint(string meetingID)
        {
            MeetingPrintingViewModel meetingPrintingViewModel = new MeetingPrintingViewModel()
            {
                MemberEntries = this.meetingBAL.MembersPrint(meetingID)
            };
            //List<MemberEntry> memberdetails = new List<MemberEntry>();
           // memberdetails = 
           
            return View("MembersListPrint", meetingPrintingViewModel);
        }
    
    }
}