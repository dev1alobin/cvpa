﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cvpa.BALs;
using Cvpa.Models;
using Cvpa.Utils;
using System.Text;
using System.IO;
using System.Net;


namespace Cvpa.Controllers
{
    public class AccountController : Controller
    {
        private AccountBAL accountBAL = new AccountBAL();
        private CategoryBAL categoryBAL = new CategoryBAL();
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(AccountController));
        private static String Send_Sms_To_Members = ControllerUtils.GetScreenData("Sms", "Send_Sms");
        // GET: /Account/


        public ActionResult IncomeIndex(AccountListViewModel accountListViewModel)
        {
            var accountFilter = accountListViewModel.AccountFilter;
            if (accountFilter == null)
            {
                accountFilter = new AccountFilterModel();
            }
            if (!String.IsNullOrEmpty(accountFilter.FromDate))
            {
                accountFilter.FromDateTime = DateTime.ParseExact(accountFilter.FromDate, "dd/MM/yyyy", null);
            }
            if (!String.IsNullOrEmpty(accountFilter.ToDate))
            {
                accountFilter.ToDateTime = DateTime.ParseExact(accountFilter.ToDate, "dd/MM/yyyy", null);
            }
            accountFilter.AccountType = "Credit";
            accountListViewModel.AccountFilter = accountFilter;
            accountListViewModel.Accounts = accountBAL.PagedQuery(accountListViewModel);// IncomePagedQuery(accountListViewModel);
            return View(accountListViewModel);

        }
        public ActionResult ExpenseIndex(AccountListViewModel accountListViewModel)
        {
            var accountFilter = accountListViewModel.AccountFilter;
            if (accountFilter == null)
            {
                accountFilter = new AccountFilterModel();
            }
            if (!String.IsNullOrEmpty(accountFilter.FromDate))
            {
                accountFilter.FromDateTime = DateTime.ParseExact(accountFilter.FromDate, "dd/MM/yyyy", null);
            }
            if (!String.IsNullOrEmpty(accountFilter.ToDate))
            {
                accountFilter.ToDateTime = DateTime.ParseExact(accountFilter.ToDate, "dd/MM/yyyy", null);
            }
            accountFilter.AccountType = "Debit";
            accountListViewModel.AccountFilter = accountFilter;
            accountListViewModel.Accounts = accountBAL.PagedQuery(accountListViewModel); //ExpensePagedQuery(accountListViewModel);
            return View(accountListViewModel);

        }

        public ActionResult IncomeCreate(AccountEditViewModel accountEditViewModel)
        {
            logger.Info("Inside DailyIncome");
            var categorytype = "Account";
            var categories = this.categoryBAL.GetCategoriesByType(categorytype);
            accountEditViewModel = new AccountEditViewModel
            {
                AccountData = new Account(),
                Categorys = categories
            };
            return View(accountEditViewModel);
        }
        public ActionResult ExpenseCreate(AccountEditViewModel accountEditViewModel)
        {
            logger.Info("Inside DailyIncome");
            var categorytype = "Account";
            var categories = this.categoryBAL.GetCategoriesByType(categorytype);
            accountEditViewModel = new AccountEditViewModel
            {
                AccountData = new Account(),
                Categorys = categories
            };
            return View(accountEditViewModel);
        }

        public ActionResult IncomeEdit(AccountListViewModel accountListViewModel)
        {
            logger.Info("Inside DailyIncome");
            accountListViewModel.Accounts = accountBAL.IncomePagedQuery(accountListViewModel);
            return View(accountListViewModel);

        }
        [HttpPost]
        public ActionResult Create(Account account)
        {
            var viewpage = "";
            try
            {
                if (account.AccountID > 0)
                {
                    logger.Info("AccountID = " + account.ReceiptID);
                    account.Status = 1;
                    this.accountBAL.Update(account);
                }
                else
                {
                    account.Status = 1;
                    this.accountBAL.Save(account);
                }
               
                if (account.PaymentType == "Credit")
                {
                    /*if (Send_Sms_To_Members == "Yes")
                    {
                        if ((account.MobileNo != null) && (account.MobileNo != null))
                        {

                            var Message = "பா.குப்புசாமி புதுபேட்டை சங்கம்,நன்கொடை வழங்கியதற்கு நன்றி";
                            logger.Info("Sending Donation Receipt SMS To Name = " + account.Name + " MobileNo = " + account.MobileNo);
                            string url = "http://dnd.alobin.com/api/sentsms.php?username=alobin&api_password=a0f5a75416&to=" + account.MobileNo + "&priority=2&sender=MCPPNS&message=" + Message;

                            HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
                            webReq.Method = "GET";
                            HttpWebResponse webResponse = (HttpWebResponse)webReq.GetResponse();

                            //I don't use the response for anything right now. But I might log the response answer later on.   
                            Stream answer = webResponse.GetResponseStream();
                            StreamReader _recivedAnswer = new StreamReader(answer);

                        }
                    }*/
                    viewpage = "IncomeIndex";
                    Auth.SetStatusMessage("Income Receipt Saved Successfully");
                }
                else
                {
                    viewpage = "ExpenseIndex";
                    Auth.SetStatusMessage("Expense Receipt Saved Successfully");
                }
            }
            catch (Exception e)
            {
                logger.Info("Error in Create Income" + e.Message);
                Auth.SetStatusMessage("Problem in Saving Receipt, please try again.", "Warning");
            }
           
            return new RedirectResult(viewpage);
        }

        //[HttpPost]
        public ActionResult Edit(long? AccountID)
        {
            var viewpage ="";
            if (AccountID != null)
            {
                long ID = (long)AccountID;
                Account account = this.accountBAL.FindByID(ID);
                var categories = this.categoryBAL.GetCategoriesByType("Account");
                AccountEditViewModel accountEditViewModel = new AccountEditViewModel
                {
                    AccountData = account,
                    Categorys = categories
                };

                if (account.PaymentType == "Credit")
                {
                    viewpage = "IncomeEdit";
                }
                else {
                    viewpage = "ExpenseEdit";
                }
                return View(viewpage, accountEditViewModel);
            }
            return RedirectToAction("IncomeIndex","Acount");
        }

        
        public ActionResult Search(AccountListViewModel accountListViewModel)
        {
            var accountFilter = accountListViewModel.AccountFilter;
            if (accountFilter == null)
            {
                accountFilter = new AccountFilterModel();
            }
            if (!String.IsNullOrEmpty(accountFilter.FromDate))
            {
                accountFilter.FromDateTime = DateTime.ParseExact(accountFilter.FromDate, "dd/MM/yyyy", null);
            }
            if (!String.IsNullOrEmpty(accountFilter.ToDate))
            {
                accountFilter.ToDateTime = DateTime.ParseExact(accountFilter.ToDate, "dd/MM/yyyy", null);
            }
            accountListViewModel.AccountFilter = accountFilter;
            var viewpage = "";
            if (!String.IsNullOrEmpty(accountFilter.AccountType) && accountFilter.AccountType.Equals("Credit"))
                viewpage = "IncomeIndex";
            else if (!String.IsNullOrEmpty(accountFilter.AccountType) && accountFilter.AccountType.Equals("Debit"))
                viewpage = "ExpenseIndex";

            accountListViewModel.Accounts = accountBAL.PagedQuery(accountListViewModel);
            return View(viewpage, accountListViewModel);
            /*logger.Info("PaymentType = "+accountListViewModel.PaymentType);
            var viewpage = "";
            if (accountListViewModel.PaymentType == "Credit")
            {
                logger.Info("Credit ");
                accountListViewModel.Accounts = accountBAL.IncomePagedQuery(accountListViewModel);
                viewpage = "IncomeIndex";
            }
            else if (accountListViewModel.PaymentType == "Debit")
            {
              logger.Info("Debit ");
                accountListViewModel.Accounts = accountBAL.ExpensePagedQuery(accountListViewModel);
                viewpage = "ExpenseIndex";
            }
            return View(viewpage, accountListViewModel);*/
        }

        public JsonResult Delete(long ID)
        {
            logger.Info("Delete Account " + ID);
            Account account = this.accountBAL.FindByID(ID);
            if (account != null)
                this.accountBAL.Delete(account);
            else
                ViewBag.Message = "Receipt is not available to delete.";
            return Json(account, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);

        }

        /*public ActionResult IncomeReceiptPrint(string Query, string PrintOption, string FromDate, string ToDate)
        {
            List<Account> incomereceiptdetails = new List<Account>();
            incomereceiptdetails = this.accountBAL.IncomeReceiptPrint(Query, FromDate, ToDate);
            logger.Info("Inside IncomeReceiptPrint Print Receipt List");
            return View("IncomeReceiptListPrint", incomereceiptdetails);

        }*/
        public ActionResult AccountsPrint(AccountListViewModel accountListViewModel)
        {
            List<Account> incomereceiptdetails = new List<Account>();
            incomereceiptdetails = this.accountBAL.AccountsPrint(accountListViewModel);
            logger.Info("Inside IncomeReceiptPrint Print Receipt List");
            return View("IncomeReceiptListPrint", incomereceiptdetails);

        }
        public ActionResult IncomeReceiptSinglePrint(long ReceiptID)
        {
            try
            {
                Account incomeid = new Account();

                if (ReceiptID > 0)
                {
                    logger.Info("Inside IncomeReceiptSinglePrint ReceiptID = " + ReceiptID);
                    incomeid = this.accountBAL.FindByID(ReceiptID);

                }
                logger.Info("IncomeReceiptSinglePrint Result" + incomeid.ToString());
                AccountEditViewModel memberEditViewModel = new AccountEditViewModel
                {
                    AccountData = incomeid
                };

                return View("IncomeReceiptSinglePrint", memberEditViewModel);
            }
            catch (Exception e)
            {
                logger.Info("Error in ReceiptSinglePrint" + e.Message);
            }
            return View("IncomeReceiptSinglePrint", new AccountEditViewModel());
        }

        public ActionResult IncomeSinglePrintByBillNo(long BillNo)
        {
            try
            {
                Account incomeid = new Account();

                if (BillNo > 0)
                {
                    logger.Info("Inside IncomeSinglePrintByBillNo ReceiptID = " + BillNo);
                    incomeid = this.accountBAL.FindByIncomeBillNo(BillNo);

                }
                logger.Info("IncomeReceiptSinglePrint Result" + incomeid.ToString());
                AccountEditViewModel memberEditViewModel = new AccountEditViewModel
                {
                    AccountData = incomeid
                };

                return View("IncomeReceiptSinglePrint", memberEditViewModel);
            }
            catch (Exception e)
            {
                logger.Info("Error in IncomeSinglePrintByBillNo" + e.Message);
            }
            return View("IncomeReceiptSinglePrint", new AccountEditViewModel());
        }

        public ActionResult ExpenseListPrint(string Query, string PrintOption, string FromDate, string ToDate)
        {
            List<Account> expensedetails = new List<Account>();
            expensedetails = this.accountBAL.ExpenseReceiptPrint(Query, FromDate, ToDate);
            logger.Info("Inside IncomeReceiptPrint Print Receipt List");
            return View("ExpenseListPrint", expensedetails);
        }

        public ActionResult ExpenseSinglePrint(long ReceiptID)
        {
            try
            {
                Account account = new Account();

                if (ReceiptID > 0)
                {
                    logger.Info("Inside VoucherSinglePrint MemberID = " + ReceiptID);
                    account = this.accountBAL.FindByID(ReceiptID);

                }
                logger.Info("VoucherSinglePrint Result" + account.ToString());
                AccountEditViewModel accountEditViewModel = new AccountEditViewModel
                {
                    AccountData = account
                };

                return View("ExpenseSinglePrint", accountEditViewModel);
            }
            catch (Exception e)
            {
                logger.Info("Error in ExpenseVoucherSinglePrint" + e.Message);
            }
            return View("ExpenseSinglePrint", new AccountEditViewModel());
        }

        public ActionResult ExpenseSinglePrintByBillNo(long BillNo)
        {
            try
            {
                Account account = new Account();

                if (BillNo > 0)
                {
                    logger.Info("Inside VoucherSinglePrint MemberID = " + BillNo);
                    account = this.accountBAL.FindByExpenseBillNo(BillNo);

                }
                logger.Info("VoucherSinglePrint Result" + account.ToString());
                AccountEditViewModel accountEditViewModel = new AccountEditViewModel
                {
                    AccountData = account
                };

                return View("ExpenseSinglePrint", accountEditViewModel);
            }
            catch (Exception e)
            {
                logger.Info("Error in ExpenseVoucherSinglePrint" + e.Message);
            }
            return View("ExpenseSinglePrint", new AccountEditViewModel());
        }
    }
}