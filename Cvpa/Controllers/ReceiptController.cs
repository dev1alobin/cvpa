﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cvpa.BALs;
using Cvpa.Models;
using AloFramework.Core.BALs;
using Cvpa.Utils;
using System.Text;
using log4net;
using System.IO;
using System.Net;

namespace Cvpa.Controllers
{
    public class ReceiptController : Controller
    {
        private TranslationBAL _transactionBAL = new TranslationBAL();
        private MemberBAL memberBAL = new MemberBAL();
        private CategoryBAL categoryBAL = new CategoryBAL();
        private ScreencustomizationBAL screencustomizationBAL = new ScreencustomizationBAL();
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(ReceiptController));
        private static String Send_Sms_To_Members = ControllerUtils.GetScreenData("Sms", "Send_Sms");
        //
        // GET: /Receipt/
        private ReceiptBAL receiptBAL = new ReceiptBAL();
        //
        // GET: /Receipt/
        public ActionResult Index(ReceiptListViewModel receiptListViewModel)
        {
            var receiptFilter = receiptListViewModel.ReceiptFilter;
            if (receiptFilter == null)
            {
                receiptFilter = new FilterModel();
            }
            if (!String.IsNullOrEmpty(receiptFilter.FromDate))
            {
                receiptFilter.FromDateTime = DateTime.ParseExact(receiptFilter.FromDate, "dd/MM/yyyy", null);
            }
            if (!String.IsNullOrEmpty(receiptFilter.ToDate))
            {
                receiptFilter.ToDateTime = DateTime.ParseExact(receiptFilter.ToDate, "dd/MM/yyyy", null);
            }
            receiptListViewModel.ReceiptFilter = receiptFilter;
            receiptListViewModel.Receipts = receiptBAL.PagedQuery(receiptListViewModel);
            return View(receiptListViewModel);
        }

        // GET: /Receipt/Create
        public ActionResult Create(ReceiptEditViewModel receiptEditViewModel)
        {
            var categorytype = "Receipt";
            var categories = this.categoryBAL.GetCategoriesByType(categorytype);
            receiptEditViewModel = new ReceiptEditViewModel
            {
                ReceiptData = new Receipt(),
                Categorys = categories
            };
            ViewData["Start_Date"] = screencustomizationBAL.GetScreenData("MemberExpiry", "Start_Date");
            ViewData["End_Date"] = screencustomizationBAL.GetScreenData("MemberExpiry", "End_Date");
            // string viewName = locale.Equals("en") ? "Create" : "CreateTamil";
            return View(receiptEditViewModel);
        }

        private void GetViewModel(Receipt modelData = null)
        {
            if (modelData != null)
            {

            }
        }

        // GET: /Receipt/Edit
        public ActionResult Edit(long? ReceiptID)
        {
            if (!ReceiptID.Equals(null))
            {
                logger.Info("Inside Eidt Receipt " + ReceiptID);

                long ID = (long)ReceiptID;
                Receipt receiptid = new Receipt();


                receiptid = this.receiptBAL.FindByID(ID);
                var categories = this.categoryBAL.GetCategoriesByType("Receipt");
                ReceiptEditViewModel receiptEditViewModel = new ReceiptEditViewModel
                {
                    ReceiptData = receiptid,
                    Categorys = categories
                };
                ViewData["Start_Date"] = screencustomizationBAL.GetScreenData("MemberExpiry", "Start_Date");
                ViewData["End_Date"] = screencustomizationBAL.GetScreenData("MemberExpiry", "End_Date");
                return View("Edit", receiptEditViewModel);

            }

            return RedirectToAction("Create", "Receipt");
        }

        [HttpPost]
        public ActionResult Create(Receipt receipt)
        {

            if (receipt.ReceiptID > 0)
            {
                // Receipt receiptid = new Receipt();
                // receiptid = this.receiptBAL.FindByID(receipt.ReceiptID);
                //   logger.Info("Inside Update Receipt " + receiptid);
                receipt.Status = 1;
                this.receiptBAL.Update(receipt);
                //ViewBag.Message = "Record Updated Successfully";
                Auth.SetStatusMessage("Receipt Updated Successfully");
                return new RedirectResult("Index");
            }
            else if (!receipt.MemberID.Equals(null))
            {
                logger.Info("Inside Save Receipt");
                logger.Info("Receipt Date = " + receipt.ReceiptDate);
                receipt.Status = 1;
                this.receiptBAL.Save(receipt);
                //if (Send_Sms_To_Members.Equals("Yes"))
                //{
                //    logger.Info("Inside Receipt Send SMS");
                //  //  if ((!receipt..Equals(null)) && (!receipt.PhoneNo.Equals("")))
                //  //  {

                //        var Message = "பா.குப்புசாமி புதுபேட்டை சங்கம்,உறுப்பினர் சந்தா கட்டியதற்கு நன்றி";
                //      //  logger.Info("Sending Receipt SMS To Member Name = "+ +" MobileNo = " + receipt.PhoneNo);
                //        string url = "http://dnd.alobin.com/api/sentsms.php?username=alobin&api_password=a0f5a75416&to=" + receipt.ReceiptNo + "&priority=2&sender=MCPPNS&message=" + Message;

                //        HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
                //        webReq.Method = "GET";
                //        HttpWebResponse webResponse = (HttpWebResponse)webReq.GetResponse();

                //        //I don't use the response for anything right now. But I might log the response answer later on.   
                //        Stream answer = webResponse.GetResponseStream();
                //        StreamReader _recivedAnswer = new StreamReader(answer);

                //        //RedirectToAction("ReceiptSMS", "Sms");
                //  //  }
                //}
                Auth.SetStatusMessage("Receipt Saved Successfully");
                return new RedirectResult("Index");               
            }
            else
            {
                //ViewBag.Message = "Please fill up all the details and enter the save button";
                Auth.SetStatusMessage("Please fill up all the details and enter the save button.", "Warning");
                return new RedirectResult("Create");

            }
        }

        [HttpPost]
        public ActionResult Update(Receipt receipt)
        {
            this.receiptBAL.Save(receipt);
            return View();
        }


        public JsonResult Delete(int ID)
        {
            Receipt receipt = this.receiptBAL.FindByID(ID);
            if (receipt != null)
                this.receiptBAL.Delete(receipt);
            else
                ViewBag.Message = "Receipt is not available to delete.";
            return Json(receipt, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Show()
        {
            return View();
        }
        public ActionResult Search(ReceiptListViewModel receiptListViewModel)
        {
            receiptListViewModel.Receipts = receiptBAL.PagedQuery(receiptListViewModel); // Todo: for future filter operation: memberListViewModel
            return View("Index", receiptListViewModel);
        }

        public JsonResult GetMembershipNo(long term)
        {
            var memberships = memberBAL.GetMembershipNo(term);
            foreach (var membership in memberships)
            {
                membership.DateOfBirthStr = membership.DateOfBirth.ToString("dd/MM/yyyy");
            }
            return Json(memberships, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReceiptPrint(ReceiptListViewModel receiptListViewModel)
        {
            List<Receipt> receiptdetails = new List<Receipt>();
            receiptdetails = this.receiptBAL.ReceiptPrint(receiptListViewModel);
            logger.Info("Inside ReceiptPrint Print Receipt List");
            return View("ReceiptListPrint", receiptdetails);

        }

        public ActionResult ReceiptSinglePrint(long ReceiptID, long MemberID)
        {

            try
            {
                Receipt receiptid = new Receipt();
                if (ReceiptID > 0)
                {
                    logger.Info("Inside ReceiptSinglePrint ReceiptID = " + ReceiptID);
                    receiptid = this.receiptBAL.ReceiptSinglePrint(ReceiptID, MemberID);
                }
                else if (MemberID > 0)
                {
                    logger.Info("Inside ReceiptSinglePrint MemberID = " + MemberID);
                    receiptid = this.receiptBAL.ReceiptSinglePrint(ReceiptID, MemberID);

                }
                logger.Info("ReceiptSingprint Result" + receiptid.ToString());
                ReceiptEditViewModel receiptEditViewModel = new ReceiptEditViewModel
                {
                    ReceiptData = receiptid
                };

                ViewData["Start_Date"] = screencustomizationBAL.GetScreenData("MemberExpiry", "Start_Date");
                ViewData["End_Date"] = screencustomizationBAL.GetScreenData("MemberExpiry", "End_Date");
                return View("ReceiptSinglePrint", receiptEditViewModel);
            }
            catch (Exception e)
            {
                logger.Info("Error in ReceiptSinglePrint" + e.Message);
            }
            return View("ReceiptSinglePrint", new ReceiptEditViewModel());
        }



    }
}

