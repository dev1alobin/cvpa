﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cvpa.BALs;
using Cvpa.Models;
using AloFramework.Core.BALs;
using Cvpa.Utils;
using System.Text;
using log4net;
using System.IO;
using System.Net;

namespace Cvpa.Controllers
{
    public class InsuranceController : Controller
    {
        private TranslationBAL _transactionBAL = new TranslationBAL();
        private MemberBAL memberBAL = new MemberBAL();
        private ScreencustomizationBAL screencustomizationBAL = new ScreencustomizationBAL();
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(InsuranceController));
        private static String Send_Sms_To_Members = ControllerUtils.GetScreenData("Sms", "Send_Sms");
        //
        // GET: /Insurance/
        private InsuranceBAL insuranceBAL = new InsuranceBAL();
        //
        // GET: /Insurance/
        public ActionResult Index(InsuranceListViewModel insuranceListViewModel)
        {
            var insuranceFilter = insuranceListViewModel.InsuranceFilter;
            if (insuranceFilter == null)
            {
                insuranceFilter = new FilterModel();
            }
            if (!String.IsNullOrEmpty(insuranceFilter.FromDate))
            {
                insuranceFilter.FromDateTime = DateTime.ParseExact(insuranceFilter.FromDate, "dd/MM/yyyy", null);
            }
            if (!String.IsNullOrEmpty(insuranceFilter.ToDate))
            {
                insuranceFilter.ToDateTime = DateTime.ParseExact(insuranceFilter.ToDate, "dd/MM/yyyy", null);
            }
            insuranceListViewModel.InsuranceFilter = insuranceFilter;
            insuranceListViewModel.Insurances = insuranceBAL.PagedQuery(insuranceListViewModel);
            return View(insuranceListViewModel);
        }

        // GET: /Insurance/Create
        public ActionResult Create(string locale = "en")
        {
            InsuranceEditViewModel insuranceEditViewModel = new InsuranceEditViewModel
            {
                InsuranceData = new Insurance()
            };
            string viewName = locale.Equals("en") ? "Create" : "CreateTamil";
            return View(viewName, insuranceEditViewModel);
        }

        private void GetViewModel(Insurance modelData = null)
        {
            if (modelData != null)
            {
                
            }
        }

        // GET: /Insurance/Edit
        public ActionResult Edit(long? InsuranceID)
        {
            if (!InsuranceID.Equals(null))
            {
                logger.Info("Inside Eidt Insurance " + InsuranceID);

                long ID = (long)InsuranceID;
                Insurance receiptid = new Insurance();
                receiptid = this.insuranceBAL.FindByID(ID);
                InsuranceEditViewModel receiptEditViewModel = new InsuranceEditViewModel
                {
                    InsuranceData = receiptid
                };
                ViewData["Start_Date"] = screencustomizationBAL.GetScreenData("MemberExpiry", "Start_Date");
                ViewData["End_Date"] = screencustomizationBAL.GetScreenData("MemberExpiry", "End_Date");
                return View("Edit", receiptEditViewModel);
                
            }
            
            return RedirectToAction("Create","Insurance");
        }

        [HttpPost]
        public ActionResult Create(Insurance insurance)
        {
            try
            {
               // DateTime dob = DateTime.ParseExact(insurance.DateOfBirthStr, "dd/MM/yyyy", null);
               // insurance.DateOfBirth = dob;
                if (insurance.InsuranceID > 0)
                {
                    // Insurance receiptid = new Insurance();
                    // receiptid = this.receiptBAL.FindByID(receipt.InsuranceID);
                    logger.Info("Inside Update Insurance ");
                    insurance.Status = 1;
                    this.insuranceBAL.Update(insurance);
                    //ViewBag.Message = "Record Updated Successfully";
                    Auth.SetStatusMessage("Insurance Receipt Updated Successfully");
                    return new RedirectResult("Index");
                }
                else if (!insurance.MemberNo.Equals(null))
                {
                    logger.Info("Inside Save Insurance");
                    logger.Info("Insurance ReceiptDate = " + insurance.ReceiptDate);
                    insurance.Status = 1;
                    this.insuranceBAL.Save(insurance);
                    /* if (Send_Sms_To_Members == "Yes")
                     {
                         if ((!insurance.PhoneNo.Equals(null)) && (!insurance.PhoneNo.Equals("")))
                         {

                             var Message = "பா.குப்புசாமி புதுபேட்டை சங்கம்,உறுப்பினர் சந்தா கட்டியதற்கு நன்றி";
                             logger.Info("Sending Insurance SMS To Member Name = " + insurance.MemberName + " MobileNo = " + insurance.PhoneNo);
                             string url = "http://dnd.alobin.com/api/sentsms.php?username=alobin&api_password=a0f5a75416&to=" + insurance.PhoneNo + "&priority=2&sender=MCPPNS&message=" + Message;

                             HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
                             webReq.Method = "GET";
                             HttpWebResponse webResponse = (HttpWebResponse)webReq.GetResponse();

                             //I don't use the response for anything right now. But I might log the response answer later on.   
                             Stream answer = webResponse.GetResponseStream();
                             StreamReader _recivedAnswer = new StreamReader(answer);

                             //RedirectToAction("InsuranceSMS", "Sms");
                         }
                     }*/

                    Auth.SetStatusMessage("Insurance Receipt Saved Successfully");
                }
                else
                {
                    //ViewBag.Message = "Please fill up all the details and enter the save button";                    
                    return new RedirectResult("Create");
                }
            }
            catch (Exception e) {
                logger.Info("Error in Save Insurance " + e.Message);
                Auth.SetStatusMessage("Problem in Saving Insurance Receipt, please try again.", "Warning");
            }
            return new RedirectResult("Index");
       }

        [HttpPost]
        public ActionResult Update(Insurance receipt)
        {
            this.insuranceBAL.Save(receipt);
            return View();
        }

        
        public JsonResult Delete(int ID)
        {
            Insurance receipt = this.insuranceBAL.FindByID(ID);
            if (receipt != null)
                this.insuranceBAL.Delete(receipt);
            else
                ViewBag.Message = "Insurance is not available to delete.";
            return Json(receipt,"application/json",Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Show()
        {
            return View();
        }
        public ActionResult Search(InsuranceListViewModel insuranceListViewModel)
        {
            insuranceListViewModel.Insurances = insuranceBAL.PagedQuery(insuranceListViewModel); // Todo: for future filter operation: memberListViewModel
            return View("Index", insuranceListViewModel);
        }

        public JsonResult GetMembershipNo(long term)
        {
            return Json(memberBAL.GetMembershipNo(term), "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        /*public ActionResult InsuranceReceiptListPrint(string Query,string FromDate, string ToDate, string PrintOption)
        {
            List<Insurance> insurancedetails = new List<Insurance>();
            insurancedetails = this.insuranceBAL.InsuranceReceiptListPrint(Query,FromDate,ToDate);
            logger.Info("Inside InsuranceReceiptListPrint ");
            return View("InsuranceReceiptListPrint", insurancedetails);
            
        }*/

        public ActionResult InsuranceReceiptListPrint(InsuranceListViewModel insuranceListVIewModel)
        {
            List<Insurance> insurancedetails = new List<Insurance>();
            insurancedetails = this.insuranceBAL.InsuranceReceiptListPrint(insuranceListVIewModel);
            logger.Info("Inside InsuranceReceiptListPrint ");
            return View("InsuranceReceiptListPrint", insurancedetails);

        }

        public ActionResult InsuranceReceiptSinglePrint(long InsuranceID,long MemberID ) {

            try
            {
                Insurance insuranceid = new Insurance();
                if (InsuranceID > 0)
                {
                    logger.Info("Inside InsuranceReceiptSinglePrint InsuranceID = " + InsuranceID);
                    insuranceid = this.insuranceBAL.InsuranceReceiptSinglePrint(InsuranceID, MemberID);
                }
                else if (MemberID > 0)
                {
                    logger.Info("Inside InsuranceReceiptSinglePrint MemberID = " + MemberID);
                    insuranceid = this.insuranceBAL.InsuranceReceiptSinglePrint(InsuranceID, MemberID);

                }
                logger.Info("InsuranceReceiptSinglePrint Result" + insuranceid.ToString());
                InsuranceEditViewModel insuranceEditViewModel = new InsuranceEditViewModel
                {
                    InsuranceData = insuranceid
                };
                return View("InsuranceReceiptSinglePrint", insuranceEditViewModel);
            }
            catch (Exception e) {
                logger.Info("Error in InsuranceReceiptSinglePrint" + e.Message);
            }
            return View("InsuranceReceiptSinglePrint", new InsuranceEditViewModel());
           }

        public ActionResult InsertInsurance()
        {
            var members = this.memberBAL.GetMembersForInsurence();
            this.insuranceBAL.InsertInsurance(members);
            return RedirectToAction("Index");
        }
        
	}
}

