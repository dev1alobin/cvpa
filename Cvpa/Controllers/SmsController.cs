﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cvpa.BALs;
using Cvpa.Models;
using System.Text;
using Cvpa.Utils;
using System.IO;
using System.Net;

namespace Cvpa.Controllers
{
    public class SmsController : Controller
    {
        private MemberBAL memberBAL = new MemberBAL();
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(SmsController));
        private static String Send_Sms_To_Members = ControllerUtils.GetScreenData("Sms", "Send_Sms");
        public ActionResult SendSms()
        {
            return View();
        }

        public JsonResult Send(string SmsTo, string Area, string MobileNumber, string Message)
        {
            logger.Info("Inside Send Festival Sms SmsTo = " + SmsTo +" Message = " + Message+" Area = " +Area);
            string result = "";
            try
            {
                logger.Info("Screencustomization value for Sending SMS = " + Send_Sms_To_Members);
                if (Send_Sms_To_Members.Equals("Yes"))
                {
                    
                        logger.Info("Send Festival Sms to all members");
                        List<Member> memberdetails = new List<Member>();
                        memberdetails = this.memberBAL.SendSms(SmsTo , Area);

                    /*if (MobileNumber != "") {
                        logger.Info("Individual Member SMS Mobile Number = " + MobileNumber);
                        string url = "http://dnd.alobin.com/api/sentsms.php?username=alobin&api_password=a0f5a75416&to=" + MobileNumber + "&priority=2&sender=MCPPNS&message=" + Message;

                        HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
                        webReq.Method = "GET";
                        HttpWebResponse webResponse = (HttpWebResponse)webReq.GetResponse();

                        //I don't use the response for anything right now. But I might log the response answer later on.   
                        Stream answer = webResponse.GetResponseStream();
                        StreamReader _recivedAnswer = new StreamReader(answer);

                        result = "Sms Sent Successfully";
                    }

                    logger.Info("Total Members count " + memberdetails.Count());
                    foreach (var members in memberdetails)
                    {
                        string mobileno = members.MobileNo;
                        if ((!mobileno.Equals(null)) && (!mobileno.Equals("")))
                        {

                          logger.Info("Send Festival SMS to all Members MobileNo = " + mobileno +" Religion = "+SmsTo +" Area = "+Area );

                           string url = "http://dnd.alobin.com/api/sentsms.php?username=alobin&api_password=a0f5a75416&to=" + mobileno + "&priority=2&sender=MCPPNS&message=" + Message;

                           HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
                           webReq.Method = "GET";
                           HttpWebResponse webResponse = (HttpWebResponse)webReq.GetResponse();

                           //I don't use the response for anything right now. But I might log the response answer later on.   
                           Stream answer = webResponse.GetResponseStream();
                           StreamReader _recivedAnswer = new StreamReader(answer);

                        result = "Sms Sent Successfully";
                        }
                    }*/

                }
                else {
                    result = "Sms Notification Disabled. Contact Administrator";
                    logger.Info("Sms Notification Disabled. Contact Administrator");
                    
                }
            }
            catch (Exception e) {
                logger.Info("Sending SMS to Members Failed Due to " + e.Message);
            }
            return Json(result, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult MembersAreaList(string term)
        {
            return Json(memberBAL.MembersAreaList(term), "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MembersAreaListByArea(string term, string areaType)
        {
            return Json(memberBAL.MembersAreaListByArea(term, areaType), "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
    }
}