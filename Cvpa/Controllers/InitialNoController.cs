﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cvpa.BALs;
using System.Text;

namespace Cvpa.Controllers
{
    public class InitialNoController : Controller
    {
        private InitialNoBAL initialnoBAL = new InitialNoBAL();
        // GET: /InitialNo/
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetInitialNo(string ResourceType)
        {

            return Json(initialnoBAL.GetInitialNo(ResourceType), "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
	}
}