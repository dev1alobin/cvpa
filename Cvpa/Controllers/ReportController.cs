﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cvpa.BALs;
using Cvpa.Models;
using AloFramework.Core.BALs;
using Cvpa.Utils;
using System.Text;
using log4net;
using System.IO;
using System.Net;

namespace Cvpa.Controllers
{
    public class ReportController : Controller
    {
        private AccountBAL accountBAL = new AccountBAL();
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(DashboardController));


        public ActionResult Index(AccountListViewModel accountListViewModel)
        {
            accountListViewModel.Accounts = accountBAL.GetGroupByIncomeAndExpenseDetails(accountListViewModel);
            return View(accountListViewModel);
        }

        public ActionResult GetGroupByIncomeAndExpenseDetails(AccountListViewModel accountListViewModel)
        {
            //  List<Account> accountdetails = new List<Account>();
            try
            {
                accountListViewModel.Accounts = accountBAL.GetGroupByIncomeAndExpenseDetails(accountListViewModel);
            }
            catch (Exception e)
            {
                logger.Info("Error in GetIncomeAndExpenseDetails = " + e.Message);
            }

            return View("Index", accountListViewModel);
        }

        public ActionResult GetGroupByIncomeAndExpenseDetailsPrint(string FromDate, string ToDate)
        {
            //   AccountListViewModel accountListViewModel = new AccountListViewModel();
            List<Account> accountdetails = new List<Account>();
            try
            {

                //  accountListViewModel.Accounts = accountBAL.GetIncomeAndExpenseDetails(accountListViewModel);
                accountdetails = this.accountBAL.GetGroupByIncomeAndExpenseDetailsPrint(FromDate, ToDate);
            }
            catch (Exception e)
            {
                logger.Info("Error in GetIncomeAndExpenseDetails = " + e.Message);
            }

            return View("IncomeExpensePrint", accountdetails);
        }
    }
}